import 'package:flutter/material.dart';
import 'package:medpad/login.dart';
import 'package:medpad/views/doctorPage/doctorDetails.dart';
import 'package:medpad/views/doctorPage/doctorProfile.dart';
import 'package:medpad/views/prescriptionView.dart';
import 'package:medpad/widgets/fab.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      title: 'CheckApp',

      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: Colors.blue[400],
          accentColor: Colors.green,
          backgroundColor: Colors.grey[100]

      ),
//      home: DoctorScreen(),
      home: Login(),
    );
  }
}

