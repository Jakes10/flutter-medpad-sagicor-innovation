
import 'dart:convert';

import 'package:medpad/models/Consultation.dart';
import 'package:medpad/models/Doctor.dart';
import 'package:medpad/models/Prescription.dart';
import 'package:medpad/models/Symptom.dart';
import 'package:medpad/models/User.dart';
import 'package:medpad/models/Vital.dart';
import 'package:medpad/services/apiConnection.dart';
import 'package:medpad/services/sharedPreference.dart';
import 'package:shared_preferences/shared_preferences.dart';

class   UserController{



  Future<List<User>> getUsers(String patientId) async{
    String apiUrl="getUser/"+patientId;
    try{
      final response = await ApiConnection().getData(apiUrl);

      if(response.statusCode==200){
        final List<User> users = userFromJson(response.body);
        print(response.body);
        return users;
      }else{
        return List<User>();
      }

    }catch(e){
      return List<User>();
    }

  }

  Future<List<Consultation>> getConsultation(int patientId) async{
    String apiUrl="getConsultation/";
//    print(apiUrl+patientId.toString());
    try{
      final response = await ApiConnection().getData(apiUrl+patientId.toString());

      if(response.statusCode==200){
        final List<Consultation> consultation = consultationFromJson(response.body);

        return consultation;
      }else{
        return List<Consultation>();
      }

    }catch(e){
      return List<Consultation>();
    }

  }

  Future<Consultation> getSingleConsultation(int consultationId) async{
    String apiUrl="getSingleConsultation/";
//print(apiUrl+consultationId.toString());
    try{
      final response = await ApiConnection().getData(apiUrl+consultationId.toString());

      if(response.statusCode==200){
//        Consultation consultation;

        var jsonDecode= json.decode(response.body);
        for(var a in jsonDecode){

//print("Reachhhh");

          return Consultation(
            consultationId: a["consultation_id"],
            patientId: a["patient_id"],
            doctorId: a["doctor_id"],
            doctorRemark: a["doctor_remark"],
            doctorSignature: a["doctor_signature"],
            consultationType: a["consultation_type"],
            createdAt: DateTime.parse(a["created_at"]),
            prescription: new Prescription(
              consultationId: a["consultation_id"],
              medicineCode: a["prescription"]["medicine_code"],
              prescriptionId: a["prescription"]["prescription_id"],
            ),
            vital: new Vital(
              vitalId: a["vital"]["vital_id"],
              consultationId: a["vital"]["consultation_id"],
              bloodPressure: a["vital"]["blood_pressure"],
              bodyTemperature: a["vital"]["body_temperature"],
              heartRate: a["vital"]["heart_rate"],
              respirationRate: a["vital"]["respiration_rate"],
              weight: a["vital"]["weight"],
              height: a["vital"]["height"],
              bloodType: a["vital"]["blood_type"],
            ),
//            symptom: symptomFromJson(a["symptoms"])


          );

        }

        return Consultation();

      }else{
        return Consultation();
      }

    }catch(e){
      return Consultation();
    }

  }

  Future<Consultation> getLastConsultation(int patientId) async{
    String apiUrl="getLastConsultation/";
//print(apiUrl+consultationId.toString());
    try{
      final response = await ApiConnection().getData(apiUrl+patientId.toString());

      if(response.statusCode==200){
//        Consultation consultation;

        var jsonDecode= json.decode(response.body);
        for(var a in jsonDecode){

//print("Reachhhh");

          return Consultation(
            consultationId: a["consultation_id"],
            patientId: a["patient_id"],
            doctorId: a["doctor_id"],
            doctorRemark: a["doctor_remark"],
            doctorSignature: a["doctor_signature"],
            consultationType: a["consultation_type"],
            createdAt: DateTime.parse(a["created_at"]),
            prescription: new Prescription(
//              consultationId: a["consultation_id"],
              medicineCode: a["prescription"]["medicine_code"],
              prescriptionId: a["prescription"]["prescription_id"],
            ),
            vital: new Vital(
              vitalId: a["vital"]["vital_id"],
              consultationId: a["vital"]["consultation_id"],
              bloodPressure: a["vital"]["blood_pressure"],
              bodyTemperature: a["vital"]["body_temperature"],
              heartRate: a["vital"]["heart_rate"],
              respirationRate: a["vital"]["respiration_rate"],
              weight: a["vital"]["weight"],
              height: a["vital"]["height"],
              bloodType: a["vital"]["blood_type"],
            ),
//            symptom: symptomFromJson(a["symptoms"])


          );

        }

        return Consultation();

      }else{
        return Consultation();
      }

    }catch(e){
      return Consultation();
    }

  }

  Future<List<Doctor>> getDoctor(int doctorId) async{
    String apiUrl="getDcotor/";
    print(apiUrl+doctorId.toString());
    try{
      final response = await ApiConnection().getData(apiUrl+doctorId.toString());

      print(response.Body);
      if(response.statusCode==200){
        final List<Doctor> doctor = doctorFromJson(response.body);

        return doctor;
      }else{
        return List<Doctor>();
      }

    }catch(e){
      return List<Doctor>();
    }

  }

  Future<List<Symptom>> getSymptom(int consultationId) async{
    String apiUrl="getSymptom/";
    print(apiUrl+consultationId.toString());
    try{
      final response = await ApiConnection().getData(apiUrl+consultationId.toString());

      print(response.Body);
      if(response.statusCode==200){
        final List<Symptom> symptom = symptomFromJson(response.body);

        return symptom;
      }else{
        return List<Symptom>();
      }

    }catch(e){
      return List<Symptom>();
    }

  }


  Future<bool> register(User user)async{
    var response;

    response = await new ApiConnection().postData(
        {
          "email": "${user.email}",
          "password": "${user.password}",
          "password_confirmation": "${user.password}",

          "first_name":"${user.firstName}",
          "middle_name":"${user.middleName}",
          "last_name":"${user.lastName}",
          "dob": "${user.dob}",
          "gender": "${user.gender}",
//          "occupation": "${user.occupation}",
          "nationality" : "${user.nationality}",
          "mobile" : "${user.mobile}",

          "country": "${user.patient.address.country}",
          "address_1":"${user.patient.address.address1}",
          "address_2":"${user.patient.address.address2}",
          "parish":"${user.patient.address.parish}",
          "accountType":  "Patient",
        }, "signUp");

    print(response.body);

    if(response.statusCode==200){
      return true;
    }

    return false;


  }

  Future<bool> userLogin(String email,String password)async{
    var response;

   response = await new ApiConnection().postData(
        {
          "email": email,
          "password": password,

        }, "login");

    var jsonDecode= json.decode(response.body);

    for(var a in jsonDecode) {
      SharedPreference().addPref("fullname", a["first_name"]+" "+a["last_name"]);
    }

//    print(prefs.getString("fullname"));

    if(response.body.toString().length>2){
      return true;
    }

    return false;


  }

}