import 'package:http/http.dart' as http;
import 'dart:convert';

class ApiConnection implements Exception{
  final String url="http://10.0.2.2:8000/api/";


  signIn(String username, String password) async{
    Map data ={
      'username': username,
      'password': password
    };

    var jsonData;
    var response = await http.post("http://10.0.2.2:8000/api/user/149070", body: data);

    if(response.statusCode==200){
      jsonData = json.decode(response.body);

      print(json.decode(response.body));
    }



  }

  postData(data, apiUrl) async{
    var fulUrl= url+apiUrl;
//    var response;

    try {
      return http.post(
          fulUrl,
          body: data,
          headers: _setHeaders()
      );

    } catch (e) {
      print(e);
      return null;
    }

  }

  getData(apiUrl) async{
    var response;


    var fulUrl= url+apiUrl;
    try {
      return http.get(
          fulUrl,
          headers: _setHeaders()
      );

//  _returnResponse(response);
    } catch (e) {
      print(e);
      return null;
    }


  }

  _setHeaders()=>{
    'Accept':'application/json',
  };

  dynamic _returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        print(responseJson);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }

  }

}

class AppException implements Exception {
  final _message;
  final _prefix;

  AppException([this._message, this._prefix]);

  String toString() {
    return "$_prefix$_message";
  }
}

class FetchDataException extends AppException {
  FetchDataException([String message])
      : super(message, "Error During Communication: ");
}

class BadRequestException extends AppException {
  BadRequestException([message]) : super(message, "Invalid Request: ");
}

class UnauthorisedException extends AppException {
  UnauthorisedException([message]) : super(message, "Unauthorised: ");
}

class InvalidInputException extends AppException {
  InvalidInputException([String message]) : super(message, "Invalid Input: ");
}










