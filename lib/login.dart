
import 'package:flutter/material.dart';
import 'package:medpad/theme/style.dart';
import 'package:medpad/views/signUp/loginInfo.dart';
import 'package:medpad/views/signUp/signUp.dart';
import 'package:medpad/views/PatientProfile.dart';
import 'package:medpad/views/home.dart';
import 'package:medpad/widgets/progressBar.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'controller/controller.dart';
import 'views/consultationDetails.dart';




class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  var formKey= GlobalKey<FormState>();
  TextEditingController controllerEmail = new TextEditingController();
  TextEditingController controllerPassword = new TextEditingController();


  bool _autoValidate=false;
  bool unconfirmed=true;
  bool _isLoading =false;



  @override
  Widget build(BuildContext context) {
    controllerEmail.text="sdf@sdf.com";
    controllerPassword.text= "12345";

    return new Scaffold(
      backgroundColor: Colors.white,
      body: Form(
        key: formKey,
        child: Center(
          child: ListView(

//          shrinkWrap: true,
            padding: EdgeInsets.only(left: 40, right: 40, top: 60),
            children: <Widget>[
              SizedBox(height: 80),
              Hero(
                tag: 'hero',
                child:  CircleAvatar(
                    backgroundColor: Colors.transparent,
                    radius: 50,
                    child: Image.asset('assets/images/logo.png',)
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height*0.08),

              Container(
                margin: EdgeInsets.symmetric( horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Offstage(
                      offstage: unconfirmed,
                      child: Card(
                        elevation: 5,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Text(
                            "Please check email and password and try again.",
                            style: TextStyle(fontSize: 13, color: Colors.red[900]),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 15,),

                    TextFormField(
                      controller: controllerEmail,
                        autovalidate: _autoValidate,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Email is required.';
                          }
                          if (!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)) {
                            return 'Incorrect Format.';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
//                          icon: Icon(Icons.person_outline),

                            labelText: 'Email',
                            hintText: 'Email',
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)
                            )

                        )),
                    SizedBox(height: 20,),
                    Container(
                      child: TextFormField(
                        controller: controllerPassword,
                          obscureText:true,
                          autovalidate: _autoValidate,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Password is required.';
                            }
//
                            return null;
                          },
                          decoration: InputDecoration(
//                          icon: Icon(Icons.lock),
                              labelText: 'Password',
                              hintText: 'Password',
                              contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10)
                              )

                          )),
                    )
                  ],
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Don\'t have an account ?',
                    style: writingTextStyle,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => SignUp()));
                    },
                    child: Text(
                      'Register',
                      style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontSize: 16,
                          fontWeight: FontWeight.w600),
                    ),
                  )
                ],
              ),
              SizedBox(height:MediaQuery.of(context).size.height*0.05,),

              _isLoading ? Progress(): GestureDetector(
                onTap: ()=>_login(),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(vertical: 15),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.grey.shade200,
                            offset: Offset(2, 4),
                            blurRadius: 5,
                            spreadRadius: 2)
                      ],
                      gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [Colors.green, Colors.blue])),
                  child:
                  Text(
                    'Login',
                    style: headingWhiteTitleTextStyle,
                  ),
                ),
              ),


            ],
          ),
        ),
      ),

    );
  }

  _login() async {



//                if()
    setState(() {
      _autoValidate=true;
      unconfirmed=true;

    });
    if(formKey.currentState.validate()){
      setState(() {
//        _isLoading=true;


      });
      bool response;
      response = await new UserController().userLogin(controllerEmail.text, controllerPassword.text);
      print(response);
      setState(() {
        _isLoading=false;

      });
      if(response){

        if(response==true){

            showDialog(
            context: context,
            builder: (_) => Home(),
          );
//print("Login");
        }
      }else{
        setState(() {

          unconfirmed=false;
        });

      }





    }

  }
//    var route= new MaterialPageRoute(
//        builder: (BuildContext context)=>
//        new Home()
//    );
//    Navigator.of(context).push(route);
}


//}

