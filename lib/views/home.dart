import 'package:flutter/material.dart';
import 'package:medpad/theme/style.dart';
import 'package:medpad/views/PatientProfile.dart';
import 'package:medpad/views/consultationDetails.dart';
import 'package:medpad/views/doctorVisit.dart';
import 'package:medpad/views/heath.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  PatientProfile profile;
  DoctorVisit consultation;
  Health health;
  List<Widget> pages;
  Widget currentPage;
  int currentTab=0;

  @override
  void initState(){
    profile =PatientProfile() ;
    consultation=DoctorVisit();
    health=Health();


    pages=[profile,consultation, health];

    currentPage=profile;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    TabController controller;

    return new Scaffold(

      appBar: new AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title:  Hero(
          tag: 'hero',
          child:  CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: 50,
              child: Image.asset('assets/images/logo.png',)
          ),
        ),
        actions: <Widget>[
//          IconButton(icon: Icon(Icons.settings), onPressed: null),
//          IconButton(icon: Icon(Icons.add_alert),color: Colors.white, onPressed: null),

//          IconButton(icon: Icon(Icons.language), onPressed: null)
        ],
      ),
      drawer: new Drawer(

        child: new ListView(
          children: <Widget>[

            new UserAccountsDrawerHeader(
              accountName: new Text("Chavoy Davis", style: headingWhiteTitleTextStyle,),
              accountEmail: new Text("ID: 19260121D", style: writingTextStyle ,),
              currentAccountPicture: CircleAvatar(
                backgroundColor: Colors.grey,
                child:   Image(image: AssetImage('assets/images/chavoy.png')),

              ),
            ),

            new ListTile(
              title: new Text("Menu", style: TextStyle(fontSize: 15),),
            ),
            Divider(height: 0),
            new ListTile(
              title: new Text("Account" ),
              dense: true,
              onTap: () {
                setState(() {
//                  controller.index=0;
                });
                Navigator.of(context).pop();
              },
              trailing:  Icon(Icons.home),
            ),
            Divider(height: 0),
            new ListTile(
              title: new Text("Visit" ),
              dense: true,
              onTap: () {
                setState(() {
//                  controller.index=1;
                });
                Navigator.of(context).pop();
              },
              trailing: Icon(Icons.perm_contact_calendar),
            ),

            Divider(height: 0),
            new ListTile(
              title: new Text("My Health"),
              dense: true,
              onTap: () {
                setState(() {
//                  controller.index=3;
                });
                Navigator.of(context).pop();
              },
              trailing: Icon(Icons.person_outline),
            ),
            Divider(height: 0),
            new ListTile(
              title: new Text("Personal Information"),
              dense: true,
              onTap: () {
                setState(() {
//                  controller.index=3;
                });
                Navigator.of(context).pop();
              },
              trailing: Icon(Icons.library_books),
            ),
            Divider(height: 0),
          ],

        ),
      ),
      body: currentPage,
      bottomNavigationBar: BottomNavigationBar(

        currentIndex: currentTab,
        onTap: (int index){
          setState(()=> currentTab=index);
          currentPage = pages[index];
        },
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home', style: subCardTextStyle,)
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.perm_contact_calendar),
              title: Text('Visit', style:   subCardTextStyle,)
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text('My health',  style: subCardTextStyle,)
          )

        ],
      ),
    );
  }
}
