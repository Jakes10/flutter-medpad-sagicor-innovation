import 'package:flutter/material.dart';
import 'package:medpad/theme/style.dart';


class SignUpDetail extends StatefulWidget {
  @override
  _SignUpDetailState createState() => _SignUpDetailState();
}

class _SignUpDetailState extends State<SignUpDetail> {
  @override
  Widget build(BuildContext context) {
    return  Container(

      child: Scaffold(
        appBar: AppBar(
//        leading: new IconButton(icon: new Icon(Icons.arrow_left, color: Colors.black),
//            onPressed: ()=> Navigator.of(context).pop()),
          iconTheme: IconThemeData(
            color: Theme.of(context).primaryColor, //change your color here
          ),
//          title:  Text( "Sign Up", textAlign: TextAlign.center, style: subHeadingTextStyle,),
//        automaticallyImplyLeading: false,
//        centerTitle: true,
          centerTitle: true,
          backgroundColor:Colors.grey[50],

        ),
        body: Center(
          child: Stack(

//            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Image.asset("assets/images/.jpg"),
              Center(
               child: Column(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: <Widget>[
                   Text(
                     "TELL US ABOUT YOURSELF",
                     style: subHeadingTextStyle ,
                   ),
                   Padding(
                     padding: const EdgeInsets.all(20.0),
                     child: Text(
                       "Please take the time out and fill "
                           "out this registration form. "
                           "The information will be used to help  "
                           "determine your medical health and will "
                           "be stored on your medical file."
                       ,
                       style: writingTextStyle ,
                       textAlign: TextAlign.center,
                     ),
                   ),

                 ],
               ),
              )

            ],
          ),
        ),
      ),
    );
  }
}
