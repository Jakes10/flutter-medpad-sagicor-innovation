import 'package:flutter/material.dart';
import 'package:medpad/theme/style.dart';
import 'package:medpad/views/signUp/signUp.dart';
import 'package:medpad/views/signUp/signUpDetials.dart';

class LoginInfo extends StatefulWidget {
  @override
  _LoginInfoState createState() => _LoginInfoState();
}

class _LoginInfoState extends State<LoginInfo> {
  var formKey= GlobalKey<FormState>();
  bool _autoValidate=false;
  TextEditingController controllerEmail = new TextEditingController();
  TextEditingController controllerPassword = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
//        leading: new IconButton(icon: new Icon(Icons.arrow_left, color: Colors.black),
//            onPressed: ()=> Navigator.of(context).pop()),
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor, //change your color here
        ),
        title:  Text( "Sign Up", textAlign: TextAlign.center, style: subHeadingTextStyle,),
//        automaticallyImplyLeading: false,
//        centerTitle: true,
        centerTitle: true,
        backgroundColor:Colors.grey[50],

      ),
      backgroundColor: Colors.white,
      body: Center(
        child: Form(
          key: formKey,
          child: ListView(

            padding: EdgeInsets.only(left: 40, right: 40),
            children: <Widget>[
              SizedBox(height: 60),
              Hero(
                tag: 'hero',
                child:  CircleAvatar(
                    backgroundColor: Colors.transparent,
                    radius: 60,
                    child: Image.asset('assets/images/logo.png',)
                ),
              ),
              SizedBox(height: 20),

              Container(
                margin: EdgeInsets.symmetric(vertical: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[

                    TextFormField(
                        controller: controllerEmail,
                        obscureText:false,
                        keyboardType: TextInputType.emailAddress,
                        autovalidate: _autoValidate,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Email is required.';
                          }
                          if (!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)){

                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: 'Email',
                            hintText: 'Email',
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)
                            )

                        )),
                    SizedBox(height: 15,),


                    TextFormField(
                        controller: controllerPassword,
                        obscureText:true,

                        decoration: InputDecoration(
                            labelText: 'Password',
                            hintText: 'Password',
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)
                            )

                        )),
                    SizedBox(height: 15,),

                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(20.0),
                child: GestureDetector(
                  onTap: (){

                var route= new MaterialPageRoute(
                    builder: (BuildContext context)=>
                    new SignUp()
                );
                Navigator.of(context).push(route);
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(vertical: 15),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.grey.shade200,
                              offset: Offset(2, 4),
                              blurRadius: 5,
                              spreadRadius: 2)
                        ],
                        gradient: LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [Colors.green, Colors.blue])),
                    child: Text(
                      'Next',
                      style: TextStyle(fontSize: 20, color: Colors.white),
                    ),
                  ),
                ),
              ),


            ],
          ),
        ),
      ),

    );

  }
}
