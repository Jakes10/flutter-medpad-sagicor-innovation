
import 'package:flutter/material.dart';
import 'package:medpad/controller/controller.dart';
import 'package:medpad/models/Address.dart';
import 'package:medpad/models/Patient.dart';
import 'package:medpad/models/User.dart';
import 'package:medpad/theme/style.dart';
import 'package:medpad/widgets/circularProgress.dart';
import 'package:medpad/widgets/progressBar.dart';
import 'package:medpad/widgets/successful.dart';

class UserAddress extends StatefulWidget {
  final User user;

  UserAddress({this.user});

  @override
  _UserAddressState createState() => _UserAddressState();
}

class _UserAddressState extends State<UserAddress> {
  var formKey= GlobalKey<FormState>();
  TextEditingController controllerCountry = new TextEditingController();
  TextEditingController controllerParish = new TextEditingController();
  TextEditingController controllerAddress1 = new TextEditingController();
  TextEditingController controllerAddress2= new TextEditingController();
  bool _autoValidate=false;
  bool termValue=false;
  String termError="Please agree to the terms and condtions.";
  bool termValueError=true;
  bool _isLoading=false;


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor, //change your color here
        ),
        title:  Text( "Address", textAlign: TextAlign.center, style: TextStyle(
            color: Theme.of(context).primaryColor
        )),
        centerTitle: true,
        backgroundColor:Colors.grey[50],

      ),
      backgroundColor: Colors.white,
      body: Center(
        child: Form(
          key: formKey,
          child:  ListView(

            padding: EdgeInsets.only(left: 40, right: 40),
            children: <Widget>[
              SizedBox(height: 20),
              Hero(
                tag: 'hero',
                child:  CircleAvatar(
                    backgroundColor: Colors.transparent,
                    radius: 30,
                    child: Image.asset('assets/images/logo.png',)
                ),
              ),
              SizedBox(height: 20),
//              Padding(
//                padding: const EdgeInsets.all(8.0),
//                child: Text('Please enter your address information', style: cardTextStyle,),
//              ),
//              SizedBox(height: 5),

              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextFormField(
//                      enabled: false,
                        controller: controllerCountry,
                        autovalidate: _autoValidate,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Country is required.';
                          }
                          return null;
                        },
                        obscureText:false,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            labelText: 'Country',
                            hintText: 'Country',
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)
                            )

                        )),
                    SizedBox(height: 15,),



                    TextFormField(
                        controller: controllerParish,
                        obscureText:false,
                        keyboardType: TextInputType.text,
                        autovalidate: _autoValidate,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Parish or providence is required.';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: 'Parish / Providence',
                            hintText: 'Parish / Providence',
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)
                            )

                        )),


                    SizedBox(height: 15,),

                    TextFormField(
                        controller: controllerAddress1,
                        keyboardType: TextInputType.text,
                        autovalidate: _autoValidate,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Address 1 is required.';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: 'Address 1',
                            hintText: 'Address 1',
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)
                            )

                        )),
                    SizedBox(height: 15,),

                    TextFormField(
                        controller: controllerAddress2,
                        keyboardType: TextInputType.text,
                        autovalidate: _autoValidate,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Address 2 is required.';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: 'Address 2',
                            hintText: 'Address 2',
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)
                            )

                        )),
                    SizedBox(height: 15,),


                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: <Widget>[
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: Divider(
                                thickness: 1,
                              ),
                            ),
                          ),
                          Text('Terms and Conditions'),
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: Divider(
                                thickness: 1,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                        ],
                      ),
                    ),

                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Checkbox(
                          activeColor: Theme.of(context).accentColor,
                          value: termValue,
                          onChanged: (val) {
                            if (termValue == false) {
                              setState(() {
                                termValue= true;
                                termValueError=true;//remove error message when active

                              });
                            } else if (termValue == true) {
                              setState(() {
                                termValue = false;
                                if(termValue)termValueError=true;//remove error message when active
                              });
                            }
                          },
//                          onChanged: (bool value) {
//
//                            setState(() {
//                              termValue = value;
////                              if(termValue)termValueError=true;//remove error message when active
//                            });
//                          },

                        ),
                        Text(
                          'I agree to the ',
                          style: writingTextStyle,
                        ),
//                        SizedBox(
//                          width: 5,
//                        ),
                        InkWell(
//                          onTap: () {
//                            Navigator.push(context,
//                                MaterialPageRoute(builder: (context) => SignUp()));
//                          },
                          child: Text(
                            'Terms ',
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 16,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                        Text(
                          'and ',
                          style: writingTextStyle,
                        ),
                        InkWell(
//                          onTap: () {
//                            Navigator.push(context,
//                                MaterialPageRoute(builder: (context) => SignUp()));
//                          },
                          child: Text(
                            'Conditions.',
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 16,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                      ],
                    ),
//                    SizedBox(height: 15,),
                    Offstage(
                      offstage: termValueError,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 30),
                        child: Text(termError, style: TextStyle(color: Colors.red[900], fontSize: 13),),
                      ),
                    )


                  ],
                ),
              ),

              _isLoading ? Progress(): Padding(
                padding: const EdgeInsets.all(20.0),
                child: GestureDetector(
                  onTap: ()=>_register(),
//                  {
//
////                    var route= new MaterialPageRoute(
////                        builder: (BuildContext context)=>
////                        new UserAddress()
////                    );
////                    Navigator.of(context).push(route);
//                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(vertical: 12),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.grey.shade200,
                              offset: Offset(2, 4),
                              blurRadius: 5,
                              spreadRadius: 2)
                        ],
                        gradient: LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [Colors.green, Colors.blue])),
                    child: Text(
                      'Register',
                      style: headingWhiteTitleTextStyle,
                    ),
                  ),
                ),
              ),



            ],
          ),
        ),
      ),

    );
  }

  _register() async{
    bool response;

    if(termValue==true){
      setState(() {

        termValueError=true;//remove error message when active
_isLoading=true;
      });
      if(formKey.currentState.validate()){

        User _user = new User(

            firstName: widget.user.firstName,
            middleName: widget.user.middleName,
            lastName: widget.user.lastName,
            email: widget.user.email,
            dob: widget.user.dob,
            gender: widget.user.gender,
            password: widget.user.password,
            mobile: widget.user.mobile,
            nationality: widget.user.nationality,
          patient: new Patient(
            address:   new Address(
                address1: controllerAddress1.text,
                address2: controllerAddress2.text,
                country: controllerCountry.text,
                parish: controllerParish.text
            )
          )

        );
        response = await new UserController().register(_user);
        setState(() {
        _isLoading=false;


        });

        if(response==true){
          showDialog(
            context: context,
            builder: (_) => Successful(),
          );

        }

      }

    }else{
      setState(() {
        termValueError=false;//remove error message when active
      });

    }

  }
}
