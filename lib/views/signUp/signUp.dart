

import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:medpad/models/User.dart';
import 'package:medpad/theme/style.dart';
import 'package:medpad/views/signUp/userAddress.dart';

class SignUp extends StatefulWidget {
  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  int _groupValue=3;
  bool _autoValidate=false;
  bool confirmOff=true;
  String gender;
  DateTime date;
  TimeOfDay time;
  TextEditingController controllerFirstName = new TextEditingController();
  TextEditingController controllerMiddleName = new TextEditingController();
  TextEditingController controllerLastName = new TextEditingController();
  TextEditingController controllerDOB = new TextEditingController();
  TextEditingController controllerMobile = new TextEditingController();
  TextEditingController controllerNationality = new TextEditingController();
  TextEditingController controllerEmail = new TextEditingController();
  TextEditingController controllerPassword = new TextEditingController();
  TextEditingController controllerConfirmPassword = new TextEditingController();

  bool genderController=true;

  var formKey= GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
//        leading: new IconButton(icon: new Icon(Icons.arrow_left, color: Colors.black),
//            onPressed: ()=> Navigator.of(context).pop()),
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor, //change your color here
        ),
        title:  Text( "Sign Up", textAlign: TextAlign.center, style: TextStyle(
            color: Theme.of(context).primaryColor
        )),
//        automaticallyImplyLeading: false,
//        centerTitle: true,
        centerTitle: true,
        backgroundColor:Colors.grey[50],

      ),
      backgroundColor: Colors.white,
      body: Center(
        child: Form(
          key: formKey,
          child: ListView(

            padding: EdgeInsets.only(left: 40, right: 40),
            children: <Widget>[
              SizedBox(height: 20),
              Hero(
                tag: 'hero',
                child:  CircleAvatar(
                    backgroundColor: Colors.transparent,
                    radius: 30,
                    child: Image.asset('assets/images/logo.png',)
                ),
              ),
              SizedBox(height: 15),

              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextFormField(
                        controller: controllerFirstName,
                        autovalidate: _autoValidate,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'First Name is required.';
                          }
                          return null;
                        },
                        obscureText:false,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            labelText: 'First Name',
                            hintText: 'First Name',
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)
                            )

                        )),
                    SizedBox(height: 15,),

                    TextFormField(
                        controller: controllerMiddleName,
                        obscureText:false,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                            labelText: 'Middle Name',
                            hintText: 'Middle Name (Optional)',
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)
                            )

                        )),
                    SizedBox(height: 15,),

                    TextFormField(
                        controller: controllerLastName,
                        obscureText:false,
                        keyboardType: TextInputType.text,
                        autovalidate: _autoValidate,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Last Name is required.';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: 'Last Name',
                            hintText: 'Last Name',
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)
                            )

                        )),
                    SizedBox(height: 15,),
                    new Text(
                      'Gender',
                      style: new TextStyle(fontSize: 16.0),
                    ),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[

                        new Radio(
                          activeColor: Theme.of(context).primaryColor,
                          value: 1,
                          groupValue: _groupValue,
                          onChanged: _handleRadioValueChange1,
                        ),
                        new Text(
                          'Female',
                          style: new TextStyle(fontSize: 16.0),
                        ),
                        new Radio(
                          activeColor: Theme.of(context).primaryColor,
                          value: 2,
                          groupValue: _groupValue ,
                          onChanged: _handleRadioValueChange1,
                        ),
                        new Text(
                          'Male',
                          style: new TextStyle(fontSize: 16.0),
                        ),



                      ],
                    ),
                    Offstage(
                      offstage: genderController,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Text("Gender is required.", style: TextStyle(color: Colors.red[900], fontSize: 12),),
                      ),

                    ),
                    SizedBox(height: 15,),

                    TextFormField(
                        controller: controllerDOB,
                        autovalidate: _autoValidate,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Date of Birth is required.';
                          }
//                          print(value);
//                          if (!RegExp("([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))").hasMatch(value)){
//                            return 'Invalid date format.';
//
//                          }
                          return null;
                        },
                        onTap: (){
                          DatePicker.showDatePicker(context,
                              showTitleActions: true,

                              minTime: DateTime(1960, 3, 5),
                              maxTime: DateTime(DateTime.now().year,DateTime.now().month,DateTime.now().day, ),
                              onChanged: (date) {
                                controllerDOB.text=date.toIso8601String().substring(0, 10);
                              },
                              onConfirm: (date) {
                                controllerDOB.text=date.toIso8601String().substring(0, 10);

                              }, currentTime: DateTime.now(), locale: LocaleType.en);
                        },
                        decoration: InputDecoration(
                            labelText: 'DOB',
                            hintText: 'DOB (yyyy-mm-dd)',
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)
                            )

                        )),
                    SizedBox(height: 15,),

                    TextFormField(
                        controller: controllerNationality,
                        keyboardType: TextInputType.text,
                        autovalidate: _autoValidate,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Nationality is required.';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: 'Nationailty',
                            hintText: 'Nationailty',
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)
                            )

                        )),
                    SizedBox(height: 15,),

                    TextFormField(
                        controller: controllerMobile,
                        obscureText:false,
                        autovalidate: _autoValidate,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Mobile is required.';
                          }
//
                          return null;
                        },
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            labelText: 'Mobile',
                            hintText: 'Mobile',
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)
                            )

                        )),
                    SizedBox(height: 20,),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 10),
                      child: Row(
                        children: <Widget>[
                          SizedBox(
                            width: 20,
                          ),
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: Divider(
                                thickness: 1,
                              ),
                            ),
                          ),
                          Text('Login Details'),
                          Expanded(
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10),
                              child: Divider(
                                thickness: 1,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 20,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 10,),

                    TextFormField(
                        controller: controllerEmail,
                        obscureText:false,
                        keyboardType: TextInputType.emailAddress,
                        autovalidate: _autoValidate,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Email is required.';
                          }
                          if (!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(value)) {
                            return 'Incorrect Format.';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: 'Email',
                            hintText: 'Email',
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)
                            )

                        )),
                    SizedBox(height: 15,),


                    TextFormField(
                        controller: controllerPassword,
                        obscureText:true,
                        onChanged: (value){
                          setState(() {

                            if(value.isNotEmpty){
                              confirmOff=false;
                            }else{
                              confirmOff=true;

                            }

                          });
                        },
                        autovalidate: _autoValidate,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Password is required.';
                          }
//
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: 'Password',
                            hintText: 'Password',
                            contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)
                            )

                        )),
                    SizedBox(height: 15,),

                    Offstage(
                      offstage: confirmOff,
                      child: TextFormField(
                          controller: controllerConfirmPassword,
                          obscureText:true,
                          autovalidate: _autoValidate,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Confirm Password is required.';
                            }
                            if (value.isEmpty) {
                              return 'Confirm Password is required.';
                            }
                            if (value!=controllerPassword.text) {
                              return 'Passwords dont match.';
                            }
//
                            return null;
                          },
                          decoration: InputDecoration(
                              labelText: 'Confirm Password',
                              hintText: 'Confirm Password',
                              contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10)
                              )

                          )),
                    )

//                    SizedBox(height: 15,),



                  ],
                ),
              ),

              Padding(
                padding: const EdgeInsets.all(20.0),
                child: GestureDetector(
                  onTap: ()=>_register(),
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(vertical: 15),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                              color: Colors.grey.shade200,
                              offset: Offset(2, 4),
                              blurRadius: 5,
                              spreadRadius: 2)
                        ],
                        gradient: LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [Colors.green, Colors.blue])),
                    child: Text(
                      'Next',
                      style: headingWhiteTitleTextStyle,
                    ),
                  ),
                ),
              ),


            ],
          ),
        ),
      ),

    );

  }

  void _handleRadioValueChange1(value) {
    setState(() {
      _groupValue=value;
//      print(_groupValue);
      if(_groupValue==1){
        gender="female";
        genderController=true;

      }else{
        gender="male";
        genderController=true;

      }
//
      print(gender);
    });
  }

  _register() {
    setState(() {
      //Gender Validation
      if(_groupValue==3){
        genderController=false;
      }
      _autoValidate=true;


    });
    if(formKey.currentState.validate()){

      var route= new MaterialPageRoute(
          builder: (BuildContext context)=>
          new UserAddress(user: new User(
            firstName: controllerFirstName.text,
            middleName: controllerMobile.text,
            lastName: controllerLastName.text,
            email: controllerEmail.text,
            dob: controllerDOB.text,
            gender: gender,
            password: controllerConfirmPassword.text,
            mobile: controllerMobile.text,
            nationality: controllerNationality.text
          ),)
      );
      Navigator.of(context).push(route);

    }
  }
}
