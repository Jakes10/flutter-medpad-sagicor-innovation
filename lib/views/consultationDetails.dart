import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medpad/controller/controller.dart';
import 'package:medpad/models/Consultation.dart';
import 'package:medpad/models/Patient.dart';
import 'package:medpad/theme/style.dart';
import 'package:medpad/widgets/ConsultatoinBuilder.dart';
import 'package:medpad/widgets/background.dart';

class ConsultationDetails extends StatefulWidget {
  final int consultationId;

  ConsultationDetails({this.consultationId});

  @override
  _ConsultationDetailsState createState() => _ConsultationDetailsState();
}

class _ConsultationDetailsState extends State<ConsultationDetails> {

  @override
  Widget build(BuildContext context) {

    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: new AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title:  Hero(
          tag: 'hero',
          child:  CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: 50,
              child: Image.asset('assets/images/logo.png',)
          ),
        ),
        actions: <Widget>[
//          IconButton(icon: Icon(Icons.settings), onPressed: null),
//          IconButton(icon: Icon(Icons.add_alert),color: Colors.white, onPressed: null),

//          IconButton(icon: Icon(Icons.language), onPressed: null)
        ],
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[

            Background(
              screenHeight: MediaQuery.of(context).size.height,
            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.center,
//              children: <Widget>[
//                Hero(
//                  tag: 'hero',
//                  child:  CircleAvatar(
//                      backgroundColor: Colors.transparent,
//                      radius: 60,
//                      child: Image.asset('assets/images/logo.png',)
//                  ),
//                ),
//
//
//              ],
//            ),


            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: SafeArea(
                child: Column(
                  children: <Widget>[
                    Wrap(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Center(
                            child:
                            Column(
                              children: <Widget>[
                                Text("Chavoy Davis", style: headingTextStyle),
                                Text("Consultaion with", style: writingTextStyle),
                                SizedBox(height: 10,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Text("Dr. P. Lawrence ", style: writingTextStyle),
                                    Text("On the 23 of July 2020", style: writingTextStyle),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),

//                        HistoryBuilder(),
                      ConsultationBuilder(consultationId: widget.consultationId,)
                      ],
                    )



                  ],
                ),
              ),
            ),

          ],
        ),
      ),

    );
  }


}

