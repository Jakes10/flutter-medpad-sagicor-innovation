import 'package:flutter/material.dart';
import 'package:medpad/theme/style.dart';
import 'package:medpad/views/PrescriptionDetail.dart';

class PrescriptionView extends StatefulWidget {
  @override
  _PrescriptionViewState createState() => _PrescriptionViewState();
}

class _PrescriptionViewState extends State<PrescriptionView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
//        iconTheme: IconThemeData(
//          color: Colors.black, //change your color here
//        ),
        title:  Hero(
          tag: 'hero',
          child:  CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: 50,
              child: Image.asset('assets/images/logo.png',)
          ),
        ),

      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: ListView(
          children: <Widget>[
            Center(child: Text("E-Prescriptions", style: subHeadingTextStyle,)),

            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Card(
                  child: Container(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            children: <Widget>[


                              Text("23", style: subHeadingTextStyle,),
                              Text("July", style: subHeadingTextStyle,),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Treatment"),
                              Text("Dr. P. Lawrence", ),
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(Icons.access_time,size: 18),
                                  SizedBox(width: 5,),
                                  Text("12:04 pm"),
                                ],
                              ),
                              SizedBox(height: 10,),
                              GestureDetector(
                                onTap: (){
                                  var route= new MaterialPageRoute(
                                    builder: (BuildContext context)=>
                                    new PrescriptionDetail()
                                );
                                Navigator.of(context).push(route);
                                },
                                child: Row(
                                  children: <Widget>[

                                    Text("View Details"),
                                    SizedBox(width: 5,),
                                    Icon(Icons.arrow_forward, size: 18,color: Theme.of(context).accentColor,),

                                  ],
                                ),
                              ),
                            ],
                          ),





                        ],
                      ),
                    ),
                  )
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Card(
                  child: Container(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Column(
                            children: <Widget>[

                              Text("12", style: subHeadingTextStyle,),
                              Text("May", style: subHeadingTextStyle,),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text("Dengue"),
                              Text("Dr. L. Goliding", ),
                            ],
                          ),
                          Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Icon(Icons.access_time,size: 18),
                                  SizedBox(width: 5,),
                                  Text("09:10 am"),
                                ],
                              ),
                              SizedBox(height: 10,),
                              GestureDetector(
                                onTap: (){
                                var route= new MaterialPageRoute(
                                    builder: (BuildContext context)=>
                                    new PrescriptionDetail()
                                );
                                Navigator.of(context).push(route);
                                },
                                child: Row(
                                  children: <Widget>[

                                    Text("View Details"),
                                    SizedBox(width: 5,),
                                    Icon(Icons.arrow_forward, size: 18,color: Theme.of(context).accentColor,),

                                  ],
                                ),
                              ),
                            ],
                          ),





                        ],
                      ),
                    ),
                  )
              ),
            ),

          ],

        ),
      )
    );
  }
}


//Padding(
//padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
//child: Column(
//children: <Widget>[
//Row(
//mainAxisAlignment: MainAxisAlignment.start,
//children: <Widget>[
//Text("Chavoy Davis", style: headingTextStyle, textAlign: TextAlign.start,),
//],
//),
//SizedBox(height: 10,),
//Row(
//mainAxisAlignment: MainAxisAlignment.center,
//children: <Widget>[
//Text("Please scan Qr code to fill prescription.", style: writingTextStyle, textAlign: TextAlign.start,),
//],
//),
//SizedBox(height: 10,),
//Row(
//mainAxisAlignment: MainAxisAlignment.center,
//children: <Widget>[
//Text("ID: 19260121D", style: cardTextStyle, textAlign: TextAlign.start,),
//],
//),
//SizedBox(height: 10,),
//
//Image(image: AssetImage('assets/images/pres.jpg'))
//
//],
//),
//),
//
