import 'package:flutter/material.dart';
import 'package:medpad/widgets/fab.dart';

class Health extends StatefulWidget {
  @override
  _HealthState createState() => _HealthState();
}

class _HealthState extends State<Health> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Center(
        child: ListView(

//          shrinkWrap: true,
          padding: EdgeInsets.only(left: 10, right: 10),
          children: <Widget>[
            SizedBox(height: 20),
            Card(
              child: ListTile(
                leading: Icon(Icons.voice_chat, color: Colors.orange,),
                title: Text('Medical Hisory', ),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Icons.people_outline,color: Colors.green,),
                title: Text('Family History'),
//              onTap: ,
              ),
            ),
            Card(
                child: ListTile(
                leading: Icon(Icons.tune, color: Colors.blue,),
                title: Text('Vacines'),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Icons.assignment,color: Colors.purple,),
                title: Text('Examinations'),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Icons.format_color_reset,color: Colors.red,),
                title: Text('Blood Tests'),
//              onTap: ,
              ),
            ),
            Card(
                child: ListTile(
                leading: Icon(Icons.fiber_smart_record, color: Colors.yellow,),
                title: Text('Allergies'),
              ),
            ),
            SizedBox(height: 100),

            Padding(
              padding: const EdgeInsets.only(left: 197, right: 20),
              child: Container(
                child: TextFormField(
                    obscureText:true,

                    decoration: InputDecoration(
//                          icon: Icon(Icons.lock),
                        labelText: 'Speak with an agent',
                        hintText: 'Need help',
                        contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)
                        )

                    )),
              ),
            ),
            SizedBox(height: 10),

            Align(
              alignment: Alignment.bottomRight,
              child: RawMaterialButton(

                padding: EdgeInsets.all(15.0),
                elevation: 12.0,
                onPressed: (){
//                  var route= new MaterialPageRoute(
//                      builder: (BuildContext context)=>
//                      new QrScan()
//                  );
//                  Navigator.of(context).push(route);
                },
                shape: CircleBorder(),
                fillColor: Colors.white,
                child: Icon(
                  Icons.send,
                  size: 20.0,
                  color: Colors.green,
                ),
              ),
            ),
//            Row(
//              mainAxisAlignment: MainAxisAlignment.end,
//              children: <Widget>[
//                Container(
//                  child: TextFormField(
////                      controller: controllerPassword,
//                      obscureText:true,
////                      autovalidate: _autoValidate,
//                      validator: (value) {
//                        if (value.isEmpty) {
//                          return 'Password is required.';
//                        }
////
//                        return null;
//                      },
//                      decoration: InputDecoration(
////                          icon: Icon(Icons.lock),
//                          labelText: 'Password',
//                          hintText: 'Password',
//                          contentPadding: EdgeInsets.fromLTRB(10, 10, 20, 10),
//                          border: OutlineInputBorder(
//                              borderRadius: BorderRadius.circular(10)
//                          )
//
//                      )),
//                )
////                FancyFab(),
//              ],
//            )
//            Hero(
//              tag: 'hero3',
//              child:  CircleAvatar(
//                  backgroundColor: Colors.transparent,
//                  radius: 20,
//                  child: Image.asset('assets/images/logo.png',)
//              ),
//            ),
//
//            SizedBox(height: 8,),


          ],
        ),
      ),
    );
  }
}
