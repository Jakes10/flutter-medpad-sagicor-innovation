import 'package:flutter/material.dart';
import 'package:medpad/theme/style.dart';

class PrescriptionDetail extends StatefulWidget {
  @override
  _PrescriptionDetailState createState() => _PrescriptionDetailState();
}

class _PrescriptionDetailState extends State<PrescriptionDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          centerTitle: true,
          backgroundColor: Colors.white,
//        iconTheme: IconThemeData(
//          color: Colors.black, //change your color here
//        ),
          title:  Hero(
            tag: 'hero',
            child:  CircleAvatar(
                backgroundColor: Colors.transparent,
                radius: 50,
                child: Image.asset('assets/images/logo.png',)
            ),
          ),

        ),
        body: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text("Chavoy Davis", style: headingTextStyle, textAlign: TextAlign.start,),
                      ],
                    ),
                    SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Please scan Qr code to fill prescription.", style: writingTextStyle, textAlign: TextAlign.start,),
                      ],
                    ),
                    SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("ID: 19260121D", style: cardTextStyle, textAlign: TextAlign.start,),
                      ],
                    ),
                    SizedBox(height: 10,),

                    Image(image: AssetImage('assets/images/pres.jpg'))

                  ],
                ),
              ),


            ],

          ),
        )
    );
  }
}


