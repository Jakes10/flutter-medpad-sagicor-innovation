import 'package:flutter/material.dart';
import 'package:medpad/widgets/circular_clipper.dart';
//import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
//import 'package:qr_code_scanner/qr_code_scanner.dart';
//import 'package:sculpturepark/models/sculpture_model.dart';
//import 'package:sculpturepark/screens/Animation.dart';
//import 'package:sculpturepark/screens/qrScanScreen.dart';
//import 'package:sculpturepark/widgets/circular_clipper.dart';
//import 'package:sculpturepark/widgets/content_scroll.dart';


class DoctorScreen extends StatefulWidget {
//  final Sculpture sculpture;

//  SculptureScreen({this.sculpture});

  @override
  _DoctorScreenState createState() => _DoctorScreenState();
}

class _DoctorScreenState extends State<DoctorScreen> {

  GlobalKey qrkey = GlobalKey();
//  QRViewController controller;
  bool _like=false;

  Future scanCode() async {

//    await FlutterBarcodeScanner.scanBarcode("#004297" , "Cancel", true);


  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        title:  Hero(
          tag: 'hero',
          child:  CircleAvatar(
              backgroundColor: Colors.transparent,
              radius: 50,
              child: Image.asset('assets/images/logo.png',)
          ),
        ),
        actions: <Widget>[
//          IconButton(icon: Icon(Icons.settings), onPressed: null),
//          IconButton(icon: Icon(Icons.add_alert),color: Colors.white, onPressed: null),

//          IconButton(icon: Icon(Icons.language), onPressed: null)
        ],
      ),
      backgroundColor: Colors.white,
      body: ListView(
        children: <Widget>[
          Stack(
            children: <Widget>[

              Container(
                transform: Matrix4.translationValues(0.0, 20.0, 0.0),
                color: Colors.white,
                child: Hero(
                  tag: 'assets/images/doc1.jpg',
                  child: ClipShadowPath(

                    clipper: CircularClipper(),

                    shadow: Shadow(blurRadius: 20.0, color: Colors.white),

                    child: Image(
                      height: 200.0,
                      width: double.infinity,
                      fit: BoxFit.fitHeight,
                      image: AssetImage('assets/images/doc1.jpg'),
                    ),
                  ),
                ),
              )
              ,
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    padding: EdgeInsets.only(left: 30.0),
                    onPressed: () => Navigator.pop(context),
                    icon: Icon(Icons.arrow_back),
                    iconSize: 30.0,
                    color: Colors.white,
                  ),
//                  Image(
//                    image: AssetImage('assets/images/park_logo.png'),
//                    height: 60.0,
//                    width: 150.0,
//                  ),
                  IconButton(
                    padding: EdgeInsets.only(left: 30.0),
                    onPressed: () {
                      setState(() {

                        if (_like)
                          _like=false;
                        else
                          _like=true;


                      });
                    },
                    icon: new Icon(
//                        Icons.favorite_border
                      _like ? Icons.favorite : Icons.favorite_border,
                      color:  _like ? Colors.red : null,
                    ),
                    iconSize: 30.0,
                    color: Colors.white,
                  ),
                ],
              ),


            ],
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 0.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 30.0),

                Text(
                  'Dr. Patricia Mc\'Neal',
                  style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 10.0),
                Text(
                  "Cardiologists",
                  style: TextStyle(
                    color: Colors.black54,
                    fontSize: 16.0,
                  ),
                ),
                SizedBox(height: 12.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.location_on,
                              color: Colors.green,
                              size: 36.0,
                            ),
                            Text(
                              'Address',
                              style: TextStyle(fontSize: 20,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(height: 5,),
                        Text('237, 6 Spring Rd,', style: TextStyle(fontSize: 18)),
                        Text('Kingston, W.I ', style: TextStyle(fontSize: 18)),
                        Text('Kingston 8', style: TextStyle(fontSize: 18)),

//                              Row(
//                                children: <Widget>[
//                                  new FlatButton(
//                                    child: new Text("Button text"),
//                                        onPressed: null,
//                                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0))
//                                    )
//                                ],
//                              )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(color: Colors.blue)),
                          color: Colors.white,
                          textColor: Colors.blue,
                          padding: EdgeInsets.all(8.0),
                          onPressed: () {},
                          child: Text(
                            "Website".toUpperCase(),
                            style: TextStyle(
                              fontSize: 14.0,
                            ),
                          ),
                        ),
                        SizedBox(width: 10),
                        FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(color: Colors.blue)),
                          color: Colors.white,
                          textColor: Colors.blue,
                          padding: EdgeInsets.all(8.0),
                          onPressed: () {},
                          child: Text(
                            "Locate".toUpperCase(),
                            style: TextStyle(
                              fontSize: 14.0,
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
                SizedBox(height: 25.0),
                Text(
                  "Specialist".toUpperCase(),
                  style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 10.0),
                Text(
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",

                  style: TextStyle(

                    color: Colors.black45,
                  ),
                ),
//                Container(
////                  height: 100.0,
//                  child: SingleChildScrollView(
//                    child: Text(
//                      "",
//
//                      style: TextStyle(
//                        color: Colors.black54,
//                      ),
//                    ),
//                  ),
//                ),
              ],
            ),
          ),
          SizedBox(height: 10),

//          ContentScroll(
//            images: widget.sculpture.screenshots,
//            title: 'Screenshots',
//            imageHeight: 200.0,
//            imageWidth: 250.0,
//          ),
        ],
      ),
    );
  }
}
