import 'package:flutter/material.dart';
import 'package:medpad/controller/controller.dart';
import 'package:medpad/models/User.dart';
import 'package:medpad/theme/style.dart';
import 'package:medpad/widgets/ShowRecordDialog.dart';
import 'package:medpad/widgets/background.dart';
import 'package:medpad/widgets/historyBuilder.dart';


class DoctorVisit extends StatefulWidget {
  @override
  _DoctorVisitState createState() => _DoctorVisitState();
}

class _DoctorVisitState extends State<DoctorVisit> {
  List<User> user;
  bool _loading=true;

  @override
  void initState() {
    // TODO: implement initState
    UserController().getUsers("3").then((con){
      user =con;
      //      print(_consultation,)
      setState(() {
        _loading=false;
//        date=_consultation.createdAt.toIso8601String().substring(0,10);

        //        UserController().getDoctor(doctorId);

      });
      //     User user= _users[0];
      //     print(user.firstName);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[

          Background(
            screenHeight: MediaQuery.of(context).size.height,
          ),
//          Row(
//            mainAxisAlignment: MainAxisAlignment.center,
//            children: <Widget>[
//              Hero(
//                tag: 'hero',
//                child:  CircleAvatar(
//                    backgroundColor: Colors.transparent,
//                    radius: 60,
//                    child: Image.asset('assets/images/logo.png',)
//                ),
//              ),
//
//
//            ],
//          ),

          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: SafeArea(
              child: Column(
                children: <Widget>[
                  _loading? Text(""):Wrap(
                    children: <Widget>[
                      Row(
                        children: <Widget>[

                          Expanded(
                              flex:2,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text("Welcome,", style: subHeadingTextStyle),
//                        Text(widget.patient.title+" "+widget.patient.lastName, style: TextStyle(color: Colors.blue, fontSize: 22, fontWeight: FontWeight.w900)),
//                                    Text("Chavoy Davis", style: headingTextStyle),
                                  Text(user[0].firstName+" "+user[0].lastName, style: headingTextStyle),

                                ],
                              )),
                          Expanded(
                            flex: 1,
                            child: Hero(
                              tag: 'hero1',
                              child:  CircleAvatar(

                                  backgroundColor: Colors.transparent,
                                  radius: 80,
                                  child: Image.asset('assets/images/chavoy.png',)
                              ),
                            ),
                          ),
                        ],
                      ),

                      Padding(
                        padding: const EdgeInsets.only(bottom: 10),
                        child: Center(
                          child: GestureDetector(
                            onTap: (){
                              showDialog(
                                context: context,
                                builder: (_) => ShowRecord(),
                              );
                            },
                            child: Container(
                              width: 160,
                              height: 50,
                              child: Container(
                                width: MediaQuery.of(context).size.width,
                                padding: EdgeInsets.symmetric(vertical: 15),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(Radius.circular(5)),
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                          color: Colors.grey.shade200,
                                          offset: Offset(2, 4),
                                          blurRadius: 5,
                                          spreadRadius: 2)
                                    ],
                                    gradient: LinearGradient(
                                        begin: Alignment.centerLeft,
                                        end: Alignment.centerRight,
                                        colors: [Colors.blue, Colors.blue])),
                                child:
                                Text(
                                  'Share with Dcotor',
                                  style: TextStyle(
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xFFFFFFFF),
                                  ),

                                ),
                              ),
                            ),
                          ),
                        ),
                      ),

                      HistoryBuilder(),



                    ],
                  )



                ],
              ),
            ),
          ),

        ],
      ),

    );

  }
}
