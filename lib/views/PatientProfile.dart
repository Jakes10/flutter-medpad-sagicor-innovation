import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:medpad/controller/controller.dart';
import 'package:medpad/models/Patient.dart';
import 'package:medpad/models/User.dart';
import 'package:medpad/services/sharedPreference.dart';
import 'package:medpad/theme/style.dart';
import 'package:medpad/views/doctorPage/doctorProfile.dart';
import 'package:medpad/views/prescriptionView.dart';
import 'package:medpad/widgets/LastVisit.dart';
import 'package:medpad/widgets/background.dart';
import 'package:medpad/widgets/historyBuilder.dart';
import 'package:medpad/widgets/progressBar.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PatientProfile extends StatefulWidget {
  final Patient patient;

  PatientProfile({this.patient});

  @override
  _PatientProfileState createState() => _PatientProfileState();
}

class _PatientProfileState extends State<PatientProfile> {
  List<User> user;
  bool _loading=true;
  @override
  void initState() {
    // TODO: implement initState
    UserController().getUsers("3").then((con){
      user =con;
      //      print(_consultation,)
      setState(() {
        _loading=false;
//        date=_consultation.createdAt.toIso8601String().substring(0,10);

        //        UserController().getDoctor(doctorId);

      });
      //     User user= _users[0];
      //     print(user.firstName);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {



    return new Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: <Widget>[

          Background(
            screenHeight: MediaQuery.of(context).size.height,
          ),
//          Row(
//            mainAxisAlignment: MainAxisAlignment.center,
//            children: <Widget>[
//              Hero(
//                tag: 'hero',
//                child:  CircleAvatar(
//                    backgroundColor: Colors.transparent,
//                    radius: 60,
//                    child: Image.asset('assets/images/logo.png',)
//                ),
//              ),
//
//
//            ],
//          ),

          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, ),
            child: SafeArea(
              child: SingleChildScrollView(
                child: Column(

//                crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    _loading? Text(""):  Wrap(

                      children: <Widget>[
                        Row(
                          children: <Widget>[

                            Expanded(
                                flex:2,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text("Welcome,", style: subHeadingTextStyle),
//                        Text(widget.patient.title+" "+widget.patient.lastName, style: TextStyle(color: Colors.blue, fontSize: 22, fontWeight: FontWeight.w900)),
//                                    Text("Chavoy Davis", style: headingTextStyle),
                                    Text(user[0].firstName+" "+user[0].lastName, style: headingTextStyle),

                                  ],
                                )),
                            Expanded(
                              flex: 1,
                              child: Hero(
                                tag: 'hero1',
                                child:  CircleAvatar(

                                    backgroundColor: Colors.transparent,
                                    radius: 80,
                                    child: Image.asset('assets/images/chavoy.png',)
                                ),
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10),
                          child: Text("Health History", style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF000000),
                          ),),
                        ),
                        LastVisit(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.fromLTRB(0, 30, 20, 10),
                              child: Material(
                                borderRadius: BorderRadius.circular(15),
                                shadowColor: Colors.black,
                                elevation: 5,
                                color: Colors.green[500],
                                child: MaterialButton(
                                  height: 60,

                                  onPressed: (){

                                    var route= new MaterialPageRoute(
                                        builder: (BuildContext context)=>
                                        new PrescriptionView()
                                    );
                                    Navigator.of(context).push(route);

                                  },
                                  color: Colors.grey[250],
                                  child: Text('e-Prescription', style: TextStyle(color: Colors.white, fontSize: 18 )),
                                ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.fromLTRB(0, 30, 20, 10),
                              child: Material(
                                borderRadius: BorderRadius.circular(15),
                                shadowColor: Colors.black,
                                elevation: 5,
                                color: Colors.blue[400],
                                child: MaterialButton(
                                  height: 60,

                                  onPressed: (){

                                    var route= new MaterialPageRoute(
                                        builder: (BuildContext context)=>
                                        new DoctorProfile()
                                    );
                                    Navigator.of(context).push(route);
                                  },
                                  color: Colors.grey[250],
                                  child: Text('Doc Profiles', style: TextStyle(color: Colors.white, fontSize: 18 )),
                                ),
                              ),
                            ),
                          ],
                        ),

//                      Row(
//                        mainAxisAlignment: MainAxisAlignment.center,
//                        children: <Widget>[
//
//                          Padding(
//                            padding: const EdgeInsets.all(10.0),
//
//                            child: Container(
//                              width: MediaQuery.of(context).size.width*0.30,
//                              padding: EdgeInsets.symmetric(vertical: 15),
//                              alignment: Alignment.center,
//                              decoration: BoxDecoration(
//                                  borderRadius: BorderRadius.all(Radius.circular(5)),
//                                  boxShadow: <BoxShadow>[
//                                    BoxShadow(
//                                        color: Colors.grey.shade200,
//                                        offset: Offset(2, 4),
//                                        blurRadius: 5,
//                                        spreadRadius: 2)
//                                  ],
//                                  gradient: LinearGradient(
//                                      begin: Alignment.centerLeft,
//                                      end: Alignment.centerLeft,
//                                      colors: [ Colors.green, Colors.green,])),
//                              child: Text(
//                                'Login',
//                                style: TextStyle(fontSize: 20, color: Colors.white),
//                              ),
//                            ),
//                          ),
//                          Padding(
//                            padding: const EdgeInsets.all(10.0),
//
//                            child: Container(
//                              width: MediaQuery.of(context).size.width*0.30,
//                              padding: EdgeInsets.symmetric(vertical: 15),
//                              alignment: Alignment.center,
//                              decoration: BoxDecoration(
//                                  borderRadius: BorderRadius.all(Radius.circular(5)),
//                                  boxShadow: <BoxShadow>[
//                                    BoxShadow(
//                                        color: Colors.grey.shade200,
//                                        offset: Offset(2, 4),
//                                        blurRadius: 5,
//                                        spreadRadius: 2)
//                                  ],
//                                  gradient: LinearGradient(
//                                      begin: Alignment.centerLeft,
//                                      end: Alignment.centerLeft,
//                                      colors: [Colors.green, Colors.green])),
//                              child: Text(
//                                'Login',
//                                style: TextStyle(fontSize: 20, color: Colors.white),
//                              ),
//                            ),
//                          ),
//                        ],
//                      ),

//                      HistoryBuilder(),
                      ],
                    ),

                    SizedBox(height: 30,),



                  ],
                ),
              ),
            ),
          ),

        ],
      ),

    );
  }



}

