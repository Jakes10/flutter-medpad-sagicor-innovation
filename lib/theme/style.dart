import 'package:flutter/material.dart';

final TextStyle fadedTextStyle = TextStyle(
  fontSize: 16.0,
  fontWeight: FontWeight.bold,
  color: Color(0x99FFFFFF),
);

final TextStyle headingTextStyle = TextStyle(
  fontSize: 30.0,
  fontWeight: FontWeight.bold,
  color: Colors.blue[900],
);
final TextStyle subHeadingTextStyle = TextStyle(
  fontSize: 20.0,
  fontWeight: FontWeight.bold,
  color: Colors.blue[800],
);

final TextStyle cardTextStyle = TextStyle(
  fontSize: 13.0,
  fontWeight: FontWeight.bold,
  color: Colors.blue[900],
);

final TextStyle subCardTextStyle = TextStyle(
  fontSize: 15.0,
  fontWeight: FontWeight.bold,
  color: Colors.black,
);


final TextStyle titleTextStyle = TextStyle(
  fontSize: 20.0,
  fontWeight: FontWeight.bold,
  color: Color(0xFF000000),
);

final TextStyle headingWhiteTitleTextStyle = TextStyle(
  fontSize: 26.0,
  fontWeight: FontWeight.w500,
  color: Color(0xFFFFFFFF),
);
final TextStyle contentHeading = TextStyle(
  fontSize: 15.0,
  fontWeight: FontWeight.w600,
);


final TextStyle cardInfoTextStyle = TextStyle(
  fontSize: 20.0,
  color: Colors.red[900],
);

final TextStyle writingTextStyle = TextStyle(
  fontSize: 16.0,
  color: Color(0xFF000000),
);
final TextStyle subWritingTextStyle = TextStyle(
  fontSize: 16.0,
  fontWeight: FontWeight.w800,
  color: Colors.green[800],
);

final TextStyle punchLine1TextStyle = TextStyle(
  fontSize: 28.0,
  fontWeight: FontWeight.w800,
  color: Colors.green[800],
);

final TextStyle punchLine2TextStyle = punchLine1TextStyle.copyWith(color: Color(0xFF000000));
