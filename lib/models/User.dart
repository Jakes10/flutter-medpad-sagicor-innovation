import 'dart:convert';
import 'package:medpad/models/Address.dart';
import 'package:medpad/models/Consultation.dart';
import 'package:medpad/models/Doctor.dart';
import 'package:medpad/models/Patient.dart';
import 'package:medpad/models/Pharmacist.dart';

List<User> userFromJson(String str) => List<User>.from(json.decode(str).map((x) => User.fromJson(x)));

String userToJson(List<User> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class User {
  User({
    this.userId,
    this.firstName,
    this.middleName,
    this.lastName,
    this.password,
    this.dob,
    this.gender,
    this.nationality,
    this.mobile,
    this.email,
    this.patient,
//    this.doctor,
//    this.pharmacist,
  });

  int userId;
  String firstName;
  String middleName;
  String lastName;
  String password;
  String dob;
  String gender;
  String nationality;
  String occupation;
  String mobile;
  String email;
  Patient patient;
//  Doctor doctor;
//  Pharmacist pharmacist;

  factory User.fromJson(Map<String, dynamic> json){ return User(
    userId: json["user_id"],
    firstName: json["first_name"],
    middleName: json["middle_name"],
    lastName: json["last_name"],
    dob: json["dob"],
    gender: json["gender"],
    nationality: json["nationality"],
    mobile: json["mobile"],
    email: json["email"],
    password: json["password"],
    patient: json["patient"] == null ? null : Patient.fromJson(json["patient"]),

//    doctor: Doctor.fromJson(json["doctor"]),
//    pharmacist: Pharmacist.fromJson(json["pharmacist"]),
  );}
//  factory User.fromJson(Map<String, dynamic> json){
//    if(json==null){
//      return null;
//    }else{
//      return User(patient: Patient(address: Address()));
//    }
//  }

  Map<String, dynamic> toJson() => {
    "user_id": userId,
    "first_name": firstName,
    "middle_name": middleName,
    "last_name": lastName,
    "dob": dob,
    "gender": gender,
    "nationality": nationality,
    "mobile": mobile,
    "email": email,
    "patient": patient == null ? null : patient.toJson(),
//    "doctor": doctor.toJson(),
//    "pharmacist": pharmacist.toJson(),
  };
}