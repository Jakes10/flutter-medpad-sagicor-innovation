class Vital {
  Vital({
    this.vitalId,
    this.consultationId,
    this.bloodPressure,
    this.bodyTemperature,
    this.heartRate,
    this.respirationRate,
    this.weight,
    this.height,
    this.bloodType,
  });

  int vitalId;
  int consultationId;
  String bloodPressure;
  String bodyTemperature;
  String heartRate;
  String respirationRate;
  String weight;
  String height;
  String bloodType;

  factory Vital.fromJson(Map<String, dynamic> json) => Vital(
    vitalId: json["vital_id"],
    consultationId: json["consultation_id"],
    bloodPressure: json["blood_pressure"],
    bodyTemperature: json["body_temperature"],
    heartRate: json["heart_rate"],
    respirationRate: json["respiration_rate"],
    weight: json["weight"],
    height: json["height"],
    bloodType: json["blood_type"],
  );

  Map<String, dynamic> toJson() => {
    "vital_id": vitalId,
    "consultation_id": consultationId,
    "blood_pressure": bloodPressure,
    "body_temperature": bodyTemperature,
    "heart_rate": heartRate,
    "respiration_rate": respirationRate,
    "weight": weight,
    "height": height,
    "blood_type": bloodType,
  };
}
