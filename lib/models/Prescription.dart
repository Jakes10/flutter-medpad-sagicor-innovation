class Prescription {
  Prescription({
    this.prescriptionId,
    this.consultationId,
    this.medicineCode,
  });

  int prescriptionId;
  int consultationId;
  String medicineCode;

  factory Prescription.fromJson(Map<String, dynamic> json) => Prescription(
    prescriptionId: json["prescription_id"],
    consultationId: json["consultation_id"],
    medicineCode: json["medicine_code"],
  );

  Map<String, dynamic> toJson() => {
    "prescription_id": prescriptionId,
    "consultation_id": consultationId,
    "medicine_code": medicineCode,
  };
}