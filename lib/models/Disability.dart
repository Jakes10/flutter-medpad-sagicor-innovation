class Disability {
  Disability({
    this.disabilityId,
    this.patientId,
    this.type,
  });

  int disabilityId;
  int patientId;
  String type;

  factory Disability.fromJson(Map<String, dynamic> json) => Disability(
    disabilityId: json["disability_id"],
    patientId: json["patient_id"],
    type: json["type"],
  );

  Map<String, dynamic> toJson() => {
    "disability_id": disabilityId,
    "patient_id": patientId,
    "type": type,
  };
}
