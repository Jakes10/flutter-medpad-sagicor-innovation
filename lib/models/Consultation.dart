import 'dart:convert';
import 'package:medpad/models/Vital.dart';
import 'Prescription.dart';
import 'Symptom.dart';


List<Consultation> consultationFromJson(String str) => List<Consultation>.from(json.decode(str).map((x) => Consultation.fromJson(x)));
String consultationToJson(List<Consultation> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Consultation {
  Consultation({
    this.consultationId,
    this.patientId,
    this.doctorId,
    this.doctorRemark,
    this.createdAt,
    this.symptom,
    this.vital,
    this.prescription,
    this.consultationType,
    this.doctorSignature

  });

  int consultationId;
  int patientId;
  int doctorId;
  String doctorRemark;
  String consultationType;
  String doctorSignature;
  DateTime createdAt;
  List<Symptom> symptom;
  Vital vital;
  Prescription prescription;

  factory Consultation.fromJson(Map<String, dynamic> json) => Consultation(
    consultationId: json["consultation_id"],
    patientId: json["patient_id"],
    doctorId: json["doctor_id"],
    doctorRemark: json["doctor_remark"],
    doctorSignature: json["doctor_signature"],
    consultationType: json["consultation_type"],
    createdAt: DateTime.parse(json["created_at"]),
    symptom: json["symptom"] == null ? null :List<Symptom>.from(json["symptom"].map((x) => Symptom.fromJson(x))),
    vital: json["vital"] == null ? null :Vital.fromJson(json["vital"]),
    prescription: json["prescription"] == null ? null : Prescription.fromJson(json["prescription"]),
  );

  Map<String, dynamic> toJson() => {
    "consultation_id": consultationId,
    "patient_id": patientId,
    "doctor_id": doctorId,
    "doctor_remark": doctorRemark,
    "doctor_signature": doctorSignature,
    "consultation_type": consultationType,
    "created_at": createdAt.toIso8601String(),
    "symptom": symptom== null ? null : List<dynamic>.from(symptom.map((x) => x.toJson())),
    "vital": vital == null ? null : vital.toJson(),
    "prescription": prescription == null ? null : prescription.toJson(),
  };
}
