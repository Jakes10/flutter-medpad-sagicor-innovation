import 'dart:convert';

import 'package:medpad/models/Address.dart';

List<Doctor> doctorFromJson(String str) => List<Doctor>.from(json.decode(str).map((x) => Doctor.fromJson(x)));
String doctorToJson(List<Doctor> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Doctor {
  Doctor({
    this.doctorId,
    this.userId,
    this.officeLocation,
  });

  int doctorId;
  int userId;
  Address officeLocation;

  factory Doctor.fromJson(Map<String, dynamic> json) => Doctor(
    doctorId: json["doctor_id"],
    userId: json["user_id"],
    officeLocation: Address.fromJson(json["office_location"]),
  );

  Map<String, dynamic> toJson() => {
    "doctor_id": doctorId,
    "user_id": userId,
    "office_location": officeLocation.toJson(),
  };
}
