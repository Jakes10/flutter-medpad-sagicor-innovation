class Address {
  Address({
    this.address1,
    this.address2,
    this.parish,
    this.country,
  });

  String address1;
  String address2;
  String parish;
  String country;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
    address1: json["Address1"],
    address2: json["Address2"],
    parish: json["Parish"],
    country: json["country"],
  );

  Map<String, dynamic> toJson() => {
    "Address1": address1,
    "Address2": address2,
    "Parish": parish,
    "country": country,
  };
}