
import 'dart:convert';



List<Symptom> symptomFromJson(String str) => List<Symptom>.from(json.decode(str).map((x) => Symptom.fromJson(x)));
String symptomToJson(List<Symptom> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Symptom {
  Symptom({
    this.symptomId,
    this.consultationId,
    this.name,
  });

  int symptomId;
  int consultationId;
  String name;

  factory Symptom.fromJson(Map<String, dynamic> json) => Symptom(
    symptomId: json["symptom_id"],
    consultationId: json["consultation_id"],
    name: json["name"],
  );

  Map<String, dynamic> toJson() => {
    "symptom_id": symptomId,
    "consultation_id": consultationId,
    "name": name,
  };
}