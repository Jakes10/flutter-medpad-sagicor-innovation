import 'package:medpad/models/Address.dart';
import 'package:medpad/models/Disability.dart';
import 'dart:convert';

import 'Consultation.dart';



class Patient {


  Patient({
    this.patientId,
    this.userId,
    this.address,
    this.occupation,
    this.emergencyContact,
    this.disability,
    this.consultation,


  });

  int patientId;
  int userId;
  Address address;
  String occupation;
  String emergencyContact;
  List<Consultation> consultation;
  List<Disability> disability;

  factory Patient.fromJson(Map<String, dynamic> json) => Patient(
    patientId: json["patient_id"],
    userId: json["user_id"],
//    address: Address.fromJson(json["address"]),
    occupation: json["occupation"],
    emergencyContact: json["emergency_contact"],
//    disability: List<Disability>.from(json["disability"].map((x) => Disability.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "patient_id": patientId,
    "user_id": userId,
    "address": address.toJson(),
    "occupation": occupation,
    "emergency_contact": emergencyContact,
    "disability": List<dynamic>.from(disability.map((x) => x.toJson())),
    "consultation": List<dynamic>.from(consultation.map((x) => x.toJson())),

  };
}