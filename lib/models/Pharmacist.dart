import 'package:medpad/models/Address.dart';

class Pharmacist {
  Pharmacist({
    this.pharmacistId,
    this.userId,
    this.pharmacyAddress,
    this.pharmacyName,
  });

  int pharmacistId;
  int userId;
  Address pharmacyAddress;
  String pharmacyName;

  factory Pharmacist.fromJson(Map<String, dynamic> json) => Pharmacist(
    pharmacistId: json["pharmacist_id"],
    userId: json["user_id"],
    pharmacyAddress: Address.fromJson(json["pharmacy_address"]),
    pharmacyName: json["pharmacy_name"],
  );

  Map<String, dynamic> toJson() => {
    "pharmacist_id": pharmacistId,
    "user_id": userId,
    "pharmacy_address": pharmacyAddress.toJson(),
    "pharmacy_name": pharmacyName,
  };
}
