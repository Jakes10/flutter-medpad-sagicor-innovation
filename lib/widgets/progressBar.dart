import 'package:flutter/material.dart';
import 'package:medpad/theme/style.dart';
import 'package:progress_indicators/progress_indicators.dart';


import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:medpad/controller/controller.dart';
import 'package:medpad/models/Patient.dart';
import 'package:medpad/models/User.dart';
import 'package:medpad/services/apiConnection.dart';
import 'package:medpad/views/consultationDetails.dart';



class Progress extends StatefulWidget {
  @override
  _ProgressState createState() => _ProgressState();
}

class _ProgressState extends State<Progress> {




  @override
  Widget build(BuildContext context) {
    return new Center(
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Text('Please Wait', style: subHeadingTextStyle,),
          JumpingDotsProgressIndicator(
            fontSize: 20.0,
             color: Theme.of(context).accentColor,
            numberOfDots: 4,

          ),
//          SizedBox(height: 20.0),

        ],
      ),

    );

  }

}

