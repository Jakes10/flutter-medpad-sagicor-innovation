import 'package:flutter/material.dart';
import 'package:medpad/theme/style.dart';

class ShowRecord extends StatefulWidget {
//  final int ;
//
//  ConsultationDetails({this.consultationId});
  @override
  State<StatefulWidget> createState() => ShowRecordState();
}

class ShowRecordState extends State<ShowRecord>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;


  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        child: ScaleTransition(
          scale: scaleAnimation,
          child: Padding(
            padding: const EdgeInsets.only(left:30, right: 30),
            child: Container(
              decoration: ShapeDecoration(
                  color: Colors.grey[100],
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0))),
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child:Container(
                  height: MediaQuery.of(context).size.height*0.62,
                  width: 250,

                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Chavoy Davis",
                        style: headingTextStyle,
                      ),
                      SizedBox(height: 20,),
                      Text(
                        "Please see your QR code below.",
                        style: cardTextStyle,
                      ),
                      SizedBox(height: 20,),

                      Hero(
                        tag: 'hero',
                        child:  CircleAvatar(
                            backgroundColor: Colors.transparent,
                            radius: 100,
                            child: Image.asset('assets/images/qrcode.png',)
                        ),
                      ),
                      SizedBox(height: 20,),

                      Text(
                        "ID: 19260121D",
                        style: cardTextStyle,
                      ),
                      SizedBox(height: 20,),

                      Text(

                        "Please scan or provide the specialist with your ID# before and appointment.",
                        textAlign: TextAlign.center,
                        style: writingTextStyle,
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(40, 20, 40, 0),
                        child: Material(
                          borderRadius: BorderRadius.circular(15),
                          shadowColor: Colors.cyan,
                          elevation: 3,
                          child: MaterialButton(
                            height: 20,
                            onPressed: (){

                              Navigator.pop(context);


                            },
                            color: Colors.grey[250],
                            child: Text('Close', style: subHeadingTextStyle),
                          ),
                        ),
                      ),
                    ],


                  ),
                ),

              ),
            ),
          ),
        ),
      ),
    );
  }
}