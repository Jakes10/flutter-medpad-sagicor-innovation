
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:medpad/controller/controller.dart';
import 'package:medpad/models/Consultation.dart';
import 'package:medpad/models/Symptom.dart';
import 'package:medpad/theme/style.dart';
import 'package:medpad/views/PrescriptionDetail.dart';
import 'package:medpad/widgets/progressBar.dart';


class ConsultationBuilder extends StatefulWidget {
  final int consultationId;

  ConsultationBuilder({this.consultationId});

  @override
  _ConsultationBuilderState createState() => _ConsultationBuilderState();
}

class _ConsultationBuilderState extends State<ConsultationBuilder> {
  var consultationID="";

  Consultation _consultation;
  List<Symptom> _symptom;
  bool _loading=true;
//  var dateUtility = new DateUtil();

  @override
  void initState() {
    // TODO: implement initState
    UserController().getSingleConsultation(widget.consultationId).then((con){
      _consultation=con;
      //      print(_consultation,)
      setState(() {
        _loading=false;

        //        UserController().getDoctor(doctorId);

      });
      //     User user= _users[0];
      //     print(user.firstName);
    });

    UserController().getSymptom(widget.consultationId).then((symptom){
      _symptom=symptom;
      //      print(_consultation,)
      setState(() {
        _loading=false;

        //        UserController().getDoctor(doctorId);

      });
      //     User user= _users[0];
      //     print(user.firstName);
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return  Container(

        height: MediaQuery.of(context).size.height*0.77,
//        margin: const EdgeInsets.only(top: 10, right: 10.0, left: 10.0),
        padding: const EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).primaryColor,
              offset: Offset(0, -2), //(x,y)
              blurRadius: 2.0,
            ),
          ],
        ),
        child: _loading == true ? Progress():
        Padding(
          padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
          child: SafeArea(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(_consultation.consultationType, style: subHeadingTextStyle,),
                  SizedBox(height: 10,),
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
//                                    Icon(Icons.access_time,size: 18),
//                                    SizedBox(width: 5,),
                        Row(
                          children: <Widget>[
                            Text("Reference #: ", style: contentHeading,),
                            Text(_consultation.consultationId.toString(), style: writingTextStyle,),
                          ],
                        ),
                        SizedBox(height: 15,),
                        Text(
                            "SYMPTOMS",
                            style:subHeadingTextStyle
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Card(
                              child:
                              Text("Rash",style: writingTextStyle,),
                            ),
                            Card(
                              child:
                              Text("High Fever",style: writingTextStyle,),
                            ), Card(
                              child:
                              Text("Sore Throat", style: writingTextStyle,),
                            ),


                          ],
                        ),
                        SizedBox(height: 10,),
                        Divider(height: 2,thickness: 2),
//                        SizedBox(height: 10,),
//
//                        Text(
//                            "DOCTOR'S REMARKS",
//                            style:contentHeading
//                        ),
                        SizedBox(height: 10,),
//                        Text(_consultation.doctorRemark, style: writingTextStyle,),
//                        SizedBox(height: 5,),
//                        Divider(height: 2,thickness: 2),
                        SizedBox(height: 5,),
                        Text("Doctor's Remarks", style: subHeadingTextStyle,),
                        SizedBox(height: 5,),
                        Text(_consultation.doctorRemark, style: writingTextStyle,),

                        SizedBox(height: 10,),
                        Divider(height: 2,thickness: 2),
                        SizedBox(height: 5,),

                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  Text("Prescription", style: subHeadingTextStyle,),
                                  SizedBox(height: 5,),
                                  Text("Precription ID", style: writingTextStyle,),
                                  Text(_consultation.prescription.medicineCode, style: subWritingTextStyle,),
                                  SizedBox(height: 10,),
                                ],
                              ),

                              Column(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.fromLTRB(20, 10, 40, 20),
                                    child: Material(
                                      borderRadius: BorderRadius.circular(15),
//                                      shadowColor: Colors.black,
//                                      elevation: 5,

                                      color: Colors.grey[200],
                                      child:
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          InkWell(
                                            onTap: () {
                                              var route= new MaterialPageRoute(
                                                  builder: (BuildContext context)=>
                                                  new PrescriptionDetail()
                                              );
                                              Navigator.of(context).push(route);
                                            },
                                            child: Text(
                                              'View Prescription',
                                              style: cardTextStyle,
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),


                                ],
                              ),

                            ],
                          ),
                        ),
                        SizedBox(height: 10,),
                        Divider(height: 2,thickness: 2),
                        SizedBox(height: 5,),
//                        Text("Doctor's Remarks", style: subHeadingTextStyle,),
//                        SizedBox(height: 5,),
//                        Text(_consultation.doctorRemark, style: writingTextStyle,),
//                        SizedBox(height: 20,),

                        Text("Vital", style: subHeadingTextStyle,),
                        SizedBox(height: 10,),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text("Blood Type", style: writingTextStyle,),
                            Text(_consultation.vital.bloodType==null?"N/A":_consultation.vital.bloodType, style: cardInfoTextStyle,),
                          ],
                        ),


                        Row(
                          children: <Widget>[
                            Card(
//                    color: Colors.,
                                child: Container(
                                  width: MediaQuery.of(context).size.width*0.36,

                                  child: Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: <Widget>[
//                                    Icon(Icons.format_color_reset,size: 25, color: Colors.red,),
                                            Hero(
                                              tag: 'hero4',
                                              child:  CircleAvatar(
                                                  backgroundColor: Colors.transparent,
                                                  radius: 12,
                                                  child: Image.asset('assets/images/thermometer.png',)
                                              ),
                                            ),
                                            SizedBox(width: 5,),
                                          ],
                                        ),

                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,

                                          children: <Widget>[

                                            Text(_consultation.vital.bodyTemperature==null?"N/A":_consultation.vital.bodyTemperature, style:cardInfoTextStyle ,),
//                                    SizedBox(height: 5,),

                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                Text("TEMPERATURE", style: cardTextStyle,),
                                                SizedBox(width: 5,),

                                              ],
                                            ),
                                          ],
                                        ),





                                      ],
                                    ),
                                  ),
                                )
                            ),

                            Card(
//                    color: Colors.,
                                child: Container(
                                  width: MediaQuery.of(context).size.width*0.36,

                                  child: Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: <Widget>[
                                            Hero(
                                              tag: 'hero9',
                                              child:  CircleAvatar(
                                                  backgroundColor: Colors.transparent,
                                                  radius: 12,
                                                  child: Image.asset('assets/images/scale.png',)
                                              ),
                                            ),
                                            SizedBox(width: 5,),
                                          ],
                                        ),

                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,

                                          children: <Widget>[

                                            Text(_consultation.vital.weight==null?"N/A":_consultation.vital.weight, style:cardInfoTextStyle ,),
//                                    SizedBox(height: 5,),

                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                Text("WEIGHT", style: cardTextStyle,),
                                                SizedBox(width: 5,),

                                              ],
                                            ),
                                          ],
                                        ),





                                      ],
                                    ),
                                  ),
                                )
                            ),

                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Card(
//                    color: Colors.,
                                child: Container(
                                  width: MediaQuery.of(context).size.width*0.36,

                                  child: Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: <Widget>[
                                    Icon(Icons.format_color_reset,size: 25, color: Colors.red,),
//                                            Hero(
//                                              tag: 'hero4',
//                                              child:  CircleAvatar(
//                                                  backgroundColor: Colors.transparent,
//                                                  radius: 12,
//                                                  child: Image.asset('assets/images/thermometer.png',)
//                                              ),
//                                            ),
                                            SizedBox(width: 5,),
                                          ],
                                        ),

                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,

                                          children: <Widget>[

                                            Text(_consultation.vital.respirationRate==null?"N/A":_consultation.vital.respirationRate, style:cardInfoTextStyle ,),
//                                    SizedBox(height: 5,),

                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                Text("RESPIRATORY RATE", style: cardTextStyle,),
                                                SizedBox(width: 5,),

                                              ],
                                            ),
                                          ],
                                        ),





                                      ],
                                    ),
                                  ),
                                )
                            ),

                            Card(
//                    color: Colors.,
                                child: Container(
                                  width: MediaQuery.of(context).size.width*0.36,

                                  child: Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: <Widget>[
                                            Hero(
                                              tag: 'h',
                                              child:  CircleAvatar(
                                                  backgroundColor: Colors.transparent,
                                                  radius: 12,
                                                  child: Image.asset('assets/images/heart.png',)
                                              ),
                                            ),
                                            SizedBox(width: 5,),
                                          ],
                                        ),

                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,

                                          children: <Widget>[

                                            Text(_consultation.vital.heartRate==null?"N/A":_consultation.vital.heartRate, style:cardInfoTextStyle ,),
//                                    SizedBox(height: 5,),

                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                Text("HEART RATE", style: cardTextStyle,),
                                                SizedBox(width: 5,),

                                              ],
                                            ),
                                          ],
                                        ),





                                      ],
                                    ),
                                  ),
                                )
                            ),

                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Card(
//                    color: Colors.,
                                child: Container(
                                  width: MediaQuery.of(context).size.width*0.36,

                                  child: Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.end,

                                          children: <Widget>[
                                            Hero(
                                              tag: 'he',
                                              child:  CircleAvatar(
                                                  backgroundColor: Colors.transparent,
                                                  radius: 12,
                                                  child: Image.asset('assets/images/blood.png',)
                                              ),
                                            ),
                                            SizedBox(width: 5,),
                                          ],
                                        ),

                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,

                                          children: <Widget>[

                                            Text(_consultation.vital.bloodPressure==null?"N/A":_consultation.vital.bloodPressure, style:cardInfoTextStyle ,),
//                                    Text("pres", style:cardInfoTextStyle ,),
                                            SizedBox(height: 5,),

                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                Text("BLOOD PRESSURE", style: cardTextStyle,),
                                                SizedBox(width: 5,),

                                              ],
                                            ),
                                          ],
                                        ),





                                      ],
                                    ),
                                  ),
                                )
                            ),


                            Card(
//                    color: Colors.,
                                child: Container(
                                  width: MediaQuery.of(context).size.width*0.36,

                                  child: Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          children: <Widget>[
                                            Hero(
                                              tag: 'hero9',
                                              child:  CircleAvatar(
                                                  backgroundColor: Colors.transparent,
                                                  radius: 12,
                                                  child: Image.asset('assets/images/scale.png',)
                                              ),
                                            ),
                                            SizedBox(width: 5,),
                                          ],
                                        ),

                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,

                                          children: <Widget>[

                                            Text(_consultation.vital.height==null?"N/A":_consultation.vital.height, style:cardInfoTextStyle ,),
//                                    SizedBox(height: 5,),

                                            Row(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                Text("HEIGHT", style: cardTextStyle,),
                                                SizedBox(width: 5,),

                                              ],
                                            ),
                                          ],
                                        ),





                                      ],
                                    ),
                                  ),
                                )
                            ),

                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
//        FutureBuilder(
//          future: getUserAccount(),
//          builder: (BuildContext context, AsyncSnapshot snapshot ){
//            if(snapshot.data==null){
//              return Container(
//                child: Center(child: Text("Loading...")),
//              );
//            }
//
//            return
//        ListView.builder(
//            itemCount: _users==null?0:_users.length,
//            itemBuilder: (BuildContext context, int index){
//              User user = _users[index];
//              return  ListTile(
//                title: Text(user.firstName +" "+user.lastName),
//                subtitle: Text(user.email),
//              );
//                Card(
//                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
//                color: Colors.white,
//                elevation: 6.0,
//                child: Container(
//
//                  child: Padding(
//                    padding: EdgeInsets.all(10),
//                    child: Column(
//                      children: <Widget>[
//                        Text(user.firstName+" "+user.lastName),
//
////                        Row(
////                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
////                          children: <Widget>[
////                            Column(
////
////                              children: <Widget>[
////                                Text(user.firstName+" "+user.lastName),
////                                Text("data"),
////                              ],
////                            ),
//////                                      SizedBox(width: 20,),
////                            Column(
////                              children: <Widget>[
////                                Text("data",),
////                                Text("data"),
////                              ],
////                            ),
////                            Column(
////                              children: <Widget>[
////                                Text("data"),
////                                Text("data"),
////                              ],
////                            ),
////                            SizedBox(height: 24.0,),
////                          ],
////                        ),
////                              Container(
////                                child: Text(
////                                  "widget.cardContent",
////                                ),
////                              ),
//                      ],
//                    ),
//                  ),
//                ),
//              )


//            }

//        )
//          },
//        ),

    );

  }
}