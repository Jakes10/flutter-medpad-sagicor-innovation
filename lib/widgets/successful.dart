import 'package:flutter/material.dart';
import 'package:medpad/login.dart';

class Successful extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => SuccessfulState();
}

class SuccessfulState extends State<Successful>
    with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    super.initState();

    controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 450));
    scaleAnimation =
        CurvedAnimation(parent: controller, curve: Curves.elasticInOut);

    controller.addListener(() {
      setState(() {});
    });

    controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => Login()));
//        if(widget.register){
//          Navigator.pop(context);
//          Navigator.pushAndRemoveUntil(
//            context,
//            MaterialPageRoute(builder: (BuildContext context) => Login()),
//            ModalRoute.withName('/'),
//          );
//        }
//          Navigator.popUntil(context, ModalRoute.withName('/login'));
//          Navigator.push(
//            context,
//            MaterialPageRoute(builder: (context) => Login()),
//          );
//          Navigator.of(context).popUntil((route) => route.isFirst);
//          Navigator.pushReplacement(
//            context,
//            MaterialPageRoute(
//              settings: RouteSettings(name: "/login"),
//              builder: (context) => Login(),
//            ),
//          );
//        else {
//          Navigator.pop(context);
//          Navigator.pushAndRemoveUntil(
//            context,
//            MaterialPageRoute(builder: (BuildContext context) => Home()),
//            ModalRoute.withName('/home'),
//          );
//        }
      },
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Center(
          child: Material(
            color: Colors.transparent,
            child: ScaleTransition(
              scale: scaleAnimation,
              child: Padding(
                padding: const EdgeInsets.only(left: 40, right: 40),
                child: Container(
                  decoration: ShapeDecoration(
                      color: Colors.grey[100],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0))),
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child:Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        IconButton(
                          icon: Icon(Icons.check_box,  color: Colors.greenAccent[400], size: 32,),

                        ),
//                        widget.register==true ?
                        Expanded(
                          child: Text("Registration Succesful!", style: TextStyle(fontSize: 16),),
                        )
//                            :
//                        Expanded(
//                          child: Text("Completed successfully!", style: TextStyle(fontSize: 16),),
//                        )
                      ],


                    ),

                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );

    ;
  }
}