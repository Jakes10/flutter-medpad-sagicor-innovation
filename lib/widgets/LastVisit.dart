
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:medpad/controller/controller.dart';
import 'package:medpad/models/Consultation.dart';
import 'package:medpad/theme/style.dart';
import 'package:medpad/views/consultationDetails.dart';
import 'package:medpad/widgets/progressBar.dart';


class LastVisit extends StatefulWidget {
  @override
  _LastVisitState createState() => _LastVisitState();
}

class _LastVisitState extends State<LastVisit> {
  var consultationID="";

  Consultation _consultation;
  bool _loading=true;
String date;
  @override
  void initState() {
    // TODO: implement initState
    UserController().getSingleConsultation(3).then((con){
      _consultation=con;
      //      print(_consultation,)
      setState(() {
        _loading=false;
//        print(_consultation);
        date=_consultation.createdAt.toIso8601String().substring(0,10);

        //        UserController().getDoctor(doctorId);

      });
      //     User user= _users[0];
      //     print(user.firstName);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child:  Container(
        width: MediaQuery.of(context).size.width*0.90,
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
//                            color: Colors.w,
          elevation: 5,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.vertical(
                    top: Radius.circular(10),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.blue,

                    ),
                  ],
                ),
//                                  color: Colors.blue,
                height: 120,
                child: _loading == true ? Center(child: Text("Loading...", style: fadedTextStyle,)): Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Row(
                        children: <Widget>[

                          Expanded(
                            flex: 2,
                            child: Hero(
                              tag: 'hero2',
                              child:  CircleAvatar(

                                  backgroundColor: Colors.transparent,
                                  radius: 20,
                                  child: Image.asset('assets/images/doc2.jpg',)
                              ),
                            ),
                          ),
                          Expanded(
                              flex:8,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(_consultation.doctorSignature, style: headingWhiteTitleTextStyle),
//                                  Text("jhg. h", style: headingWhiteTitleTextStyle),
//                        Text(widget.patient.title+" "+widget.patient.lastName, style: TextStyle(color: Colors.blue, fontSize: 22, fontWeight: FontWeight.w900)),
                                ],
                              )),

                        ],
                      ),
                      SizedBox(height: 20,),
                      Text("Based on your last check up on "+date.substring(8,10)+"/"+date.substring(5,7)+"/"+date.substring(0,4)+". Your body health is in good condition.",
                          style: TextStyle(color: Colors.white, fontSize: 15)
                      ),
                    ],
                  ),
                ),
              ),
//              SizedBox(height: 10,),
              _loading == true ? Padding(
                padding: const EdgeInsets.all(20.0),
                child: Progress(),
              ):Column(
              children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[

                    Card(
//                    color: Colors.,
                        child: Container(
                          width: MediaQuery.of(context).size.width*0.36,

                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,

                                  children: <Widget>[
                                    Hero(
                                      tag: 'he',
                                      child:  CircleAvatar(
                                          backgroundColor: Colors.transparent,
                                          radius: 12,
                                          child: Image.asset('assets/images/blood.png',)
                                      ),
                                    ),
                                    SizedBox(width: 5,),
                                  ],
                                ),

                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,

                                  children: <Widget>[

                                    Text(_consultation.vital.bloodPressure==null?"N/A":_consultation.vital.bloodPressure, style:cardInfoTextStyle ,),
//                                    Text("pres", style:cardInfoTextStyle ,),
                                    SizedBox(height: 5,),

                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Text("BLOOD PRESSURE", style: cardTextStyle,),
                                        SizedBox(width: 5,),

                                      ],
                                    ),
                                  ],
                                ),





                              ],
                            ),
                          ),
                        )
                    ),
                    Card(
//                    color: Colors.,
                        child: Container(
                          width: MediaQuery.of(context).size.width*0.36,

                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Hero(
                                      tag: 'h',
                                      child:  CircleAvatar(
                                          backgroundColor: Colors.transparent,
                                          radius: 12,
                                          child: Image.asset('assets/images/heart.png',)
                                      ),
                                    ),
                                    SizedBox(width: 5,),
                                  ],
                                ),

                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,

                                  children: <Widget>[

//                                    Text("hj", style:cardInfoTextStyle ,),
                                    Text(_consultation.vital.heartRate==null?"N/A":_consultation.vital.heartRate, style:cardInfoTextStyle ,),
                                    SizedBox(height: 5,),

                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Text("HEART RATE", style: cardTextStyle,),
                                        SizedBox(width: 5,),

                                      ],
                                    ),
                                  ],
                                ),





                              ],
                            ),
                          ),
                        )
                    ),

                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[

                    Card(
//                    color: Colors.,
                        child: Container(
                          width: MediaQuery.of(context).size.width*0.36,

                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
//                                    Icon(Icons.format_color_reset,size: 25, color: Colors.red,),
                                    Hero(
                                      tag: 'hero4',
                                      child:  CircleAvatar(
                                          backgroundColor: Colors.transparent,
                                          radius: 12,
                                          child: Image.asset('assets/images/thermometer.png',)
                                      ),
                                    ),
                                    SizedBox(width: 5,),
                                  ],
                                ),

                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,

                                  children: <Widget>[

                                    Text(_consultation.vital.bodyTemperature==null?"N/A":_consultation.vital.bodyTemperature, style:cardInfoTextStyle ,),
//                                    SizedBox(height: 5,),

                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Text("TEMPERATURE", style: cardTextStyle,),
                                        SizedBox(width: 5,),

                                      ],
                                    ),
                                  ],
                                ),





                              ],
                            ),
                          ),
                        )
                    ),
                    Card(
//                    color: Colors.,
                        child: Container(
                          width: MediaQuery.of(context).size.width*0.36,

                          child: Padding(
                            padding: const EdgeInsets.all(10),
//                            padding: const EdgeInsets.all(10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: <Widget>[
                                    Hero(
                                      tag: 'hero9',
                                      child:  CircleAvatar(
                                          backgroundColor: Colors.transparent,
                                          radius: 12,
                                          child: Image.asset('assets/images/scale.png',)
                                      ),
                                    ),
                                    SizedBox(width: 5,),
                                  ],
                                ),

                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,

                                  children: <Widget>[

                                    Text(_consultation.vital.weight==null?"N/A":_consultation.vital.weight, style:cardInfoTextStyle ,),
//                                    SizedBox(height: 5,),

                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Text("WEIGHT", style: cardTextStyle,),
                                        SizedBox(width: 5,),

                                      ],
                                    ),
                                  ],
                                ),





                              ],
                            ),
                          ),
                        )
                    ),
                  ],
                ),
              ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
//                    Text(
//                      'Don\'t have an account ?',
//                      style: TextStyle(fontSize: 15, fontWeight: FontWeight.w600),
//                    ),
//                    SizedBox(
//                      width: 15,
//                    ),
                      InkWell(
                        onTap: () {
                          var route= new MaterialPageRoute(
                              builder: (BuildContext context)=>
                              new ConsultationDetails(consultationId: _consultation.consultationId,)
                          );
                          Navigator.of(context).push(route);
                        },
                        child: Text(
                          'See More Information',
                          style: cardTextStyle,
                        ),
                      )
                    ],
                  ),
                ),

              ],
            ),

            ],
          ),
        ),

      ),
    );

  }
}