import 'package:flutter/material.dart';

class CircularProgress extends StatefulWidget {

  @override
  _CircularProgressState createState() => _CircularProgressState();
}

class _CircularProgressState extends State<CircularProgress> {
  bool downloading =true;


  @override
  Widget build(BuildContext context) {
    return new Container(
      alignment: AlignmentDirectional.center,
      decoration: new BoxDecoration(
        color: Colors.white70,
      ),
      child: new Container(
        decoration: new BoxDecoration(
            color: Colors.blue[200],
            borderRadius: new BorderRadius.circular(10.0)
        ),
        width: 300.0,
        height: 200.0,
        alignment: AlignmentDirectional.center,
        child: new Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Center(
              child: new SizedBox(
                height: 50.0,
                width: 50.0,
                child: new CircularProgressIndicator(
                  value: null,
                  strokeWidth: 7.0,
                ),
              ),
            ),
            new Container(
              margin: const EdgeInsets.only(top: 25.0),
              child: new Center(
                child: new Text(
                  "loading.. wait...",
                  style: new TextStyle(
                      color: Colors.white
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
//    return Center(
//
//        child: Material(
//          color: Colors.transparent,
//          child: Center(
//            child: Container(
////                color: Colors.transparent,
//              height: 120.0,
//              width: 200.0,
//              child: Column(
//                mainAxisAlignment: MainAxisAlignment.center,
//                children: <Widget>[
//                  CircularProgressIndicator(),
////                  SizedBox(
////                    height: 20.0,
////                  ),
////                    Text(
////                      "Loading...",
////                      style: TextStyle(
////                        color: Colors.white,
////                      ),
////                    )
//                ],
//              )
//            )
//          )
//        )
//    );
  }


}
