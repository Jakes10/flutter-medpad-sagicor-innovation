
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:medpad/controller/controller.dart';
import 'package:medpad/models/Consultation.dart';
import 'package:medpad/models/Patient.dart';
import 'package:medpad/models/User.dart';
import 'package:medpad/services/apiConnection.dart';
import 'package:medpad/theme/style.dart';
import 'package:medpad/views/consultationDetails.dart';
import 'package:medpad/widgets/progressBar.dart';
import 'package:grouped_list/grouped_list.dart';


class HistoryBuilder extends StatefulWidget {
  @override
  _HistoryBuilderState createState() => _HistoryBuilderState();
}

class _HistoryBuilderState extends State<HistoryBuilder> {
  var consultationID="";
//  List<User> user;
  List<Consultation> _consultation;
  bool _loading=true;
//  var dateUtility = new DateUtil();

  @override
  void initState() {
//    print(par.);
    // TODO: implement initState
    UserController().getConsultation(1).then((con){
      _consultation=con;
      setState(() {
        _loading=false;

//        UserController().getDoctor(doctorId);

      });
//     User user= _users[0];
//     print(user.firstName);
    });



//print(_users[1].firstName);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return  Container(

        height: MediaQuery.of(context).size.height*0.50,
//        margin: const EdgeInsets.only(top: 10, right: 10.0, left: 10.0),
        padding: const EdgeInsets.only(bottom: 10,),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).primaryColor,
              offset: Offset(0, -2), //(x,y)
              blurRadius: 2.0,
            ),
          ],
        ),
        child: _loading == true ? Progress():
        ListView.builder
          (
            itemCount: _consultation.length,
            itemBuilder: (BuildContext context, int index) {
              Consultation consultations= _consultation[index];

              return
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Card(
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              children: <Widget>[

                                Text(consultations.createdAt.toIso8601String().substring(5, 7), style: subHeadingTextStyle,),
                                Text(getMonth(DateTime.parse(consultations.createdAt.toIso8601String()).month), style: subHeadingTextStyle,),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(consultations.consultationType, ),
                                Text(consultations.doctorSignature),
                              ],
                            ),
                            Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Icon(Icons.access_time,size: 18),
                                    SizedBox(width: 5,),
                                    Text(consultations.createdAt.toIso8601String().substring(11, 16)),
                                  ],
                                ),
                                SizedBox(height: 10,),
                                GestureDetector(
                                  onTap: (){
                                    var route= new MaterialPageRoute(
                                        builder: (BuildContext context)=>
                                        new ConsultationDetails(consultationId: consultations.consultationId,)
                                    );
                                    Navigator.of(context).push(route);
                                  },
                                  child: Row(
                                    children: <Widget>[

                                      Text("View Details"),
                                      SizedBox(width: 5,),
                                      Icon(Icons.arrow_forward, size: 18,color: Theme.of(context).accentColor,),

                                    ],
                                  ),
                                ),
                              ],
                            ),





                          ],
                        ),
                      ),
                    )
                  ),
                );
            }
        )
//        FutureBuilder(
//          future: getUserAccount(),
//          builder: (BuildContext context, AsyncSnapshot snapshot ){
//            if(snapshot.data==null){
//              return Container(
//                child: Center(child: Text("Loading...")),
//              );
//            }
//
//            return
//        ListView.builder(
//            itemCount: _users==null?0:_users.length,
//            itemBuilder: (BuildContext context, int index){
//              User user = _users[index];
//              return  ListTile(
//                title: Text(user.firstName +" "+user.lastName),
//                subtitle: Text(user.email),
//              );
//                Card(
//                margin: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
//                color: Colors.white,
//                elevation: 6.0,
//                child: Container(
//
//                  child: Padding(
//                    padding: EdgeInsets.all(10),
//                    child: Column(
//                      children: <Widget>[
//                        Text(user.firstName+" "+user.lastName),
//
////                        Row(
////                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
////                          children: <Widget>[
////                            Column(
////
////                              children: <Widget>[
////                                Text(user.firstName+" "+user.lastName),
////                                Text("data"),
////                              ],
////                            ),
//////                                      SizedBox(width: 20,),
////                            Column(
////                              children: <Widget>[
////                                Text("data",),
////                                Text("data"),
////                              ],
////                            ),
////                            Column(
////                              children: <Widget>[
////                                Text("data"),
////                                Text("data"),
////                              ],
////                            ),
////                            SizedBox(height: 24.0,),
////                          ],
////                        ),
////                              Container(
////                                child: Text(
////                                  "widget.cardContent",
////                                ),
////                              ),
//                      ],
//                    ),
//                  ),
//                ),
//              )


//            }

//        )
//          },
//        ),

    );

  }
  getMonth(var mn){
    var month;

    switch (mn) {
      case 1:
        month = "Jan";
        break;
      case 2:
        month = "Feb";
        break;
      case 3:
        month = "Mar";
        break;
      case 4:
        month = "Apr";
        break;
      case 5:
        month = "May";
        break;
      case 6:
        month = "Jun";
        break;
      case 7:
        month = "Jul";
        break;
      case 8:
        month = "Aug";
        break;
      case 9:
        month = "Sep";
        break;
      case 10:
        month = "Oct";
        break;
      case 11:
        month = "Nov";
        break;
      case 12:
        month = "Dec";
        break;
    }
return month;

  }
}